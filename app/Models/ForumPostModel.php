<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForumPostModel extends Model
{
    protected $table = 'forum_posts';
    protected $fillable = ['author', 'text', 'topic', 'type'];

    public function authorInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'author');
    }
}
