<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemUserModel extends Model
{
    protected  $table = 'items_users';
    protected $fillable = ['equip','type'];


}
