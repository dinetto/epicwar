<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockedIPModel extends Model
{
    protected $table = 'blocked_ip';
    protected $guarded = [];
}
