<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailMessageModel extends Model
{
    protected $table = 'mail_messages';
    protected $fillable = ['dialog', 'sender', 'message', 'read'];

    public function getDialog()
    {
        return $this->hasOne('App\Models\MailDialogModel', 'id', 'dialog');
    }

    public function getSenderInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'sender');
    }
}
