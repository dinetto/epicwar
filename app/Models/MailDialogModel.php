<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MailDialogModel extends Model
{
    protected $table = 'mail_dialogs';
    protected $fillable = ['user1', 'user2'];

    public function getDialogMessages()
    {
        return $this->hasMany('App\Models\MailMessageModel', 'dialog', 'id');
    }

    public function getUser1Info()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user1');
    }

    public function getUser2Info()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user2');
    }

}
