<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Services\Services;

class UserModel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'updated_at', 'common_chest', 'rare_chest', 'legendary_chest'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = [
        'win_rate',
        'param_summm',
    ];

    protected $table = 'users';

    public function updateOnline()
    {
        $this->online_date = time()+7200;
        return $this->save();
    }

    public function isRole($role = 'user')
    {
        if($role == 'user') return $this->role != 'user';
        else return $this->role == $role;
    }

    public function getTotalBattlesAttribute()
    {
        return $this->wins+$this->loses;
    }

    public function getWinRateAttribute()
    {
        return $this->total_battles > 0 ? ($this->wins/$this->total_battles)*100 : 0;
    }

    public function isBanned()
    {
        return $this->block_type == 'ban' && $this->block_time >= time();
    }

    public function isMuted()
    {
        return $this->block_type == 'mute' && $this->block_time >= time();
    }

    public function issetGift()
    {
        return $this->gift != '';
    }

    public function clearExpBoost()
    {
        return $this->update(['exp_boost' => 0]);
    }

    public function updateIp($request)
    {
        if($this->ip != $request->ip())
        {
            $this->ip = $request->ip();
            $this->save();
            return true;
        }
        return false;
    }

    public function getParamSummmAttribute()
    {
        $summ = Services::getAllParameters('health', $this->id)+Services::getAllParameters('attack', $this->id)+Services::getAllParameters('armor', $this->id);
        return $summ;
    }

}
