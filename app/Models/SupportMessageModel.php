<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportMessageModel extends Model
{
    protected $table = 'support_messages';
    protected $fillable = ['ticket', 'user', 'text'];

    public function getUser()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user');
    }
}
