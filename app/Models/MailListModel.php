<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailListModel extends Model
{
    protected $table = 'mail_list';
    protected $fillable = ['type', 'user1', 'user2'];

    public function getInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user2');
    }
}
