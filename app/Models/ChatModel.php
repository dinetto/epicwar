<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatModel extends Model
{
    protected $table = 'chat';
    protected $fillable = ['user', 'reply', 'message'];

    public function getSenderInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user');
    }

    public function getGetterInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'reply');
    }
}
