<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameSettingModel extends Model
{
    protected $table = 'game_settings';
    protected $fillable = ['key', 'value'];
    public $timestamps = false;
}
