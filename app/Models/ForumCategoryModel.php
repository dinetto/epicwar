<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForumCategoryModel extends Model
{
    protected $table = 'forum_categories';
    protected $fillable = ['name', 'type'];
}
