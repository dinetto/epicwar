<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferalModel extends Model
{
    protected $table = 'referals';
    protected $guarded = [];

    public function getUserInfo()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'referal');
    }
}
