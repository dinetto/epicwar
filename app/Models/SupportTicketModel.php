<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTicketModel extends Model
{
    protected $table = 'support_tickets';
    protected $fillable = ['user', 'type', 'name', 'status'];

    public function getMessages()
    {
        return $this->hasMany('App\Models\SupportMessageModel', 'ticket', 'id');
    }

    public function getUser()
    {
        return $this->hasOne('App\Models\UserModel', 'id', 'user');
    }
}
