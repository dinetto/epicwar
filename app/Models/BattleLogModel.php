<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BattleLogModel extends Model
{
    protected $table = 'battle_logs';
    protected $fillable = ['battle', 'type', 'message'];
}
