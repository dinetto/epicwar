<?php

namespace App\Providers;

use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Blade;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('ru');
        Blade::directive('sendchat', function()
        {
            return '<?php if(time()-Auth::user()->register_date > 1200): ?>';
        });
        Blade::directive('endsendchat', function()
        {
            return '<?php endif; ?>';
        });
        Blade::directive('unbanned', function()
        {
            return '<?php if(!Auth::user()->isBanned()): ?>';
        });
        Blade::directive('endunbanned', function()
        {
            return '<?php endif; ?>';
        });
        Blade::directive('muted', function()
        {
            return '<?php if(Auth::user()->isMuted()): ?>';
        });
        Blade::directive('endmuted', function()
        {
            return '<?php endif; ?>';
        });
        Blade::directive('gift', function()
        {
            return '<?php if(Auth::user()->issetGift()): ?>';
        });
        Blade::directive('endgift', function()
        {
            return '<?php endif; ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
