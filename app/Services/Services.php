<?php

namespace App\Services;

use App\Models\GameSettingModel;
use Auth;

use App\Models\UserModel;
use Carbon\CarbonInterval;
use LaravelEmojiOne;

class Services
{


    public static function setTutorialStage($stage)
    {
        $user = UserModel::find(Auth::user()->id);
        $user->tutorial_stage = $stage;
        $user->save();
    }


    public static function addMoney($gold = null, $silver = null, $user = 0)
    {
        $user = UserModel::find($user == 0 ? Auth::user()->id : $user);
        $action = GameSettingModel::where('key', 'action')->first();

        $user->gold += $gold;
        $user->silver += $silver;
        $user->save();
        return true;
    }

    //Change parametr
    public static function changeParametrs($str = null, $hp = null, $armor = null, $action = "+", $user = 0)
    {
        $user = UserModel::find($user == 0 ? Auth::user()->id : $user);
        if ($action == "+") {
            $user->strength += $str;
            $user->health += $hp;
            $user->armor += $armor;
        } else {
            $user->strength -= $str;
            $user->health -= $hp;
            $user->armor -= $armor;
        }

        $user->save();
        return true;
    }

    public static function userInfo($userId)
    {
        $user = UserModel::find($userId);
        if ($user == null) return (object)['login' => 'Удалён'];
        else return $user;
    }

    public static function getUpgradeData($level, $result = 'cost')
    {
        if ($level == 0) {
            $stat = 0;
            $cost = 0;
            $progress = 0;
        } elseif ($level == 1) {
            $stat = 3;
            $cost = 0;
            $progress = 25;
        } else {
            $stat = 0;
            $cost = 3;
            $count = 1;
            $progress = 25;
            while ($count < $level) {
                $stat += 3;
                $cost *= 2;
                $progress *= 2;
                $count++;
            }
        }
        if ($result == 'stat') return $stat;
        elseif ($result == 'progress') return $progress;
        else return $cost;
    }

    public static function getUpgradeBonus($param, $level)
    {
        $stat = self::getUpgradeData($level, 'stat');
        return round(($stat / 100) * $param);
    }

    public static function timer($time, $return = 0)
    {
        CarbonInterval::setLocale('ru');
        $d = 3600 * 24;
        $days = floor($time / $d);
        $time = $time - ($d * $days);
        $hours = floor($time / 3600);
        $time = $time - (3600 * $hours);
        $minutes = floor($time / 60);
        $time = $time - (60 * $minutes);
        $seconds = floor($time);
        if ($return == 1) return (object)['days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds];
        return CarbonInterval::create(0, 0, 0, $days, $hours, $minutes, $seconds);
    }

    public static function getChance()
    {
        $tempChance = range(0.1, 100, 0.1);
        shuffle($tempChance);
        $chance = $tempChance[array_rand($tempChance)];
        unset($tempChance);
        return $chance;
    }

    public static function getMaxCardLevel($user)
    {
        $card = CardUserModel::join('cards', 'cards.id', '=', 'cards_user.card')->where('cards_user.user', Auth::user()->id)->where('cards_user.status', 'redeemed')->orderBy('cards.level', 'DESC')->first();
        return $card->level;
    }

    public static function getPickStatus($pick, $battle, $user)
    {
        $status = ArenaCardsModel::where('user', $user)->where('battle', $battle)->where('pick', $pick)->count();
        return $status;
    }

    public static function setArenaHealth($myPick1, $myPick2, $opponentPick1, $opponentPick2, $battle)
    {
        $myPick1->health = $myPick1->getCardUserInfo->getCardInfo->health + Services::getUpgradeBonus($myPick1->getCardUserInfo->getCardInfo->health, $myPick1->getCardUserInfo->upgrade_level);
        $myPick1->save();
        $myPick2->health = $myPick2->getCardUserInfo->getCardInfo->health + Services::getUpgradeBonus($myPick2->getCardUserInfo->getCardInfo->health, $myPick2->getCardUserInfo->upgrade_level);
        $myPick2->save();
        $opponentPick1->health = $opponentPick1->getCardUserInfo->getCardInfo->health + Services::getUpgradeBonus($opponentPick1->getCardUserInfo->getCardInfo->health, $opponentPick1->getCardUserInfo->upgrade_level);
        $opponentPick1->save();
        $opponentPick2->health = $opponentPick2->getCardUserInfo->getCardInfo->health + Services::getUpgradeBonus($opponentPick2->getCardUserInfo->getCardInfo->health, $opponentPick2->getCardUserInfo->upgrade_level);
        $opponentPick2->save();
        ArenaCardsModel::where('battle', $battle->id)->update(['moved' => '0']);
        $battle->status = 'battle';
        $battle->save();
        return true;
    }

    public static function getAvailableCardLevel()
    {
        $userCards = CardUserModel::with('getCardInfo')->where('user', '=', Auth::user()->id)->where('status', '=', 'redeemed')->get();
        $issetLevels = [];
        $maxLevel = 1;
        foreach ($userCards as $uC) {
            if ($uC->getCardInfo->level > $maxLevel) $maxLevel = $uC->getCardInfo->level;
        }
        for ($i = 1; $i <= $maxLevel + 1; $i++) {
            $issetLevels[] = $i;
        }
        return $issetLevels;
    }

    public static function getChestTime($time)
    {
        $tt = self::timer($time, 1);
        if ($time >= 86400 * 2) return ($tt->days + 1) . ' д.';
        elseif ($time >= 86400 && $time < 86400 * 2) return floor($time / 86400) . ' д.';
        elseif ($time < 86400 && $time > 3599) return ($tt->hours + 1) . ' ч.';
        elseif ($time <= 3599 && $time > 59) return ($tt->minutes + 1) . ' м.';
        else return $tt->seconds . ' с.';
    }

    public static function viewNumber($number)
    {
        if ($number < 100000) return $number;
        elseif ($number >= 100000 && $number < 1000000) return floor($number / 1000) . 'k';
        elseif ($number >= 1000000 && $number < 1000000000) return floor($number / 1000000) . 'm';
        else return floor($number / 1000000000) . 'b';
    }

    public static function getAllParameters($parameter, $user)
    {
        $userInfo = UserModel::find($user);
        $deck = explode(';', $userInfo->deck);
        $health = 0;
        $attack = 0;
        $armor = 0;

        if ($parameter == 'health') return $health;
        elseif ($parameter == 'attack') return $attack;
        elseif ($parameter == 'armor') return $armor;
    }

    public static function bbcodes($string, $user = null)
    {
        $userInfo = UserModel::find($user);
        $string = htmlspecialchars($string);
        $string = LaravelEmojiOne::toImage($string);
        if ($user != null && $userInfo->role != 'user') $string = self::bbcodeConvert($string);
        $string = nl2br($string);
        return $string;
    }

    public static function bbcodeConvert($string)
    {
        $regex = '\[url=(?(?=[^\]]*\]\[\/url\])([^\]]*)\]\[\/url\] |([^\]]*)\](.*?)\[\/url\])';
        $string = preg_replace("/$regex/sux", '<a href="$1$2" target="_blank">$1$3</a>', $string);
        $string = preg_replace("/\[img\s*\]([^\]\[]+)\[\/img\]/", '<img src="$1" style="max-width: 390px;"/>', $string);
        $string = preg_replace("#\[color=(.+)\](.+)\[\/color\]#isU", '<span style="color:\\1">\\2</span>', $string);
        $string = preg_replace('~\[center\](.*?)\[\/center\]~si', '<div style="text-align: center">$1</div>', $string);
        $string = preg_replace('~\[right\](.*?)\[\/right\]~si', '<div style="text-align: right">$1</div>', $string);
        $string = preg_replace('~\[left\](.*?)\[\/left\]~si', '<div style="text-align: left">$1</div>', $string);
        return $string;
    }

    public static  function  qualityItem($quality){
       $item_quality  =  [1=>"Обычный",2=>"Редкий",3=>"Эпический",4=>"Мифический",5=>"Реликтовый"];

       return $item_quality[$quality];
    }
}
