<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\UserModel;
use Illuminate\Foundation\Auth\User;

class BlockRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = UserModel::where('login', $value)->first();
        if(!$user) return true;
        return $user->isBanned() || $user->isMuted() ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Игрок уже заблокирован!';
    }
}
