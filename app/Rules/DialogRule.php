<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;
use App\Models\MailDialogModel;
use App\Models\MailListModel;

class DialogRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mailDialog = MailDialogModel::find($value);
        if($mailDialog == null) return false;
        $opp = ($mailDialog->user1 == Auth::user()->id) ? $mailDialog->user2 : $mailDialog->user1;
        if(MailListModel::where('user1', '=', $opp)->where('user2', '=', Auth::user()->id)->where('type', '=', 'blacklist')->count() > 0) return false;
        if($mailDialog->user1 == Auth::user()->id || $mailDialog->user2 == Auth::user()->id) return true;
        else return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Вы не участвуете в диалоге!';
    }
}
