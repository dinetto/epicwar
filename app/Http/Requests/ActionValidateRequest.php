<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActionValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->route()->parameter('type');
        $result = [
            'type' => 'required|integer|in:1,2,3,4',
            'time' => 'required|integer'
        ];
        if($type == 1) $result['percent'] = 'required|integer|between:1,100';
        elseif($type == 2)
        {
            $result['summ'] = 'required|integer';
            $result['card'] = 'required|integer|exists:cards,id';
        }
        elseif($type == 3)
        {
            $result['summ'] = 'required|integer';
            $result['percent'] = 'required|integer|between:1,100';
        }
        elseif($type == 4) $result['percent'] = 'required|integer|between:1,100';
        return $result;
    }

    public function messages()
    {
        return [
            'type.required' => 'Выберите тип акции!',
            'type.integer' => 'Неверный формат!',
            'type.in' => 'Неверный формат!',
            'time.required' => 'Введите время действия акции!',
            'time.integer' => 'Неверный формат!',
            'percent.required' => 'Введите процент!',
            'percent.integer' => 'Неверный формат!',
            'percent.between' => 'Неверный формат процентов!',
            'summ.required' => 'Введите сумму к пополнению!',
            'summ.integer' => 'Неверный формат!',
            'card.required' => 'Выберите карту в подарок!',
            'card.integer' => 'Неверный формат!',
            'card.exists' => 'Карты не существует!',
        ];
    }
}
