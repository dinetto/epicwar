<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'level' => 'required|integer',
            'type' => 'required|string|in:common,premium',
            'min_attack' => 'required|integer',
            'max_attack' => 'required|integer',
            'health' => 'required|integer',
            'armor' => 'required|integer',
            'attack_count' => 'required|integer',
            'cooldown' => 'required|integer',
            'learn_time' => 'required|integer',
            'open_cost' => 'required|integer',
            'buyout_cost' => 'required|integer',
            'image' => 'required|image|mimes:jpg,jpeg,png,bmp,gif'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Введите название карты!',
            'name.string' => 'Неверный формат!',
            'level.required' => 'Введите уровень карты!',
            'level.integer' => 'Неверный формат!',
            'type.required' => 'Выберите тип карты!',
            'type.string' => 'Неверный формат!',
            'type.in' => 'Неверный формат!',
            'min_attack.required' => 'Введите минимальную атаку карты!',
            'min_attack.integer' => 'Неверный формат!',
            'max_attack.required' => 'Введите максимальную атаку карты!',
            'max_attack.integer' => 'Неверный формат!',
            'health.required' => 'Введите здоровье карты!',
            'health.integer' => 'Неверный формат!',
            'armor.required' => 'Введите защиту карты!',
            'armor.integer' => 'Неверный формат!',
            'attack_count.required' => 'Введите кол-во ударов до отката карты!',
            'attack_count.integer' => 'Неверный формат!',
            'cooldown.required' => 'Введите время отката карты!',
            'cooldown.integer' => 'Неверный формат!',
            'learn_time.required' => 'Введите время изучения карты!',
            'learn_time.integer' => 'Неверный формат!',
            'open_cost.required' => 'Введите стоимость открытия карты!',
            'open_cost.integer' => 'Неверный формат!',
            'buyout_cost.required' => 'Введите стоимость выкупа карты!',
            'buyout_cost.integer' => 'Неверный формат!',
            'image.required' => 'Выберите изображение карты!',
            'image.image' => 'Неверный формат!'
        ];
    }
}
