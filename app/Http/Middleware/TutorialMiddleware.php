<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class TutorialMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->tutorial_stage < 4 && Route::current()->uri != 'tutorial/'.Auth::user()->tutorial_stage)
        {
            return redirect('/tutorial/'.Auth::user()->tutorial_stage);
        }
        return $next($request);
    }
}
