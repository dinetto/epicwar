<?php

namespace App\Http\Middleware;

use App\Models\ForumTopicModel;
use App\Models\GameSettingModel;
use App\Models\MailDialogModel;
use App\Models\NotificationModel;
use App\Models\UserModel;
use Closure;
use Auth;
use View;
use URL;
use Carbon\Carbon;
use Route;

class OnlineMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Auth::user()->updateOnline();
        Auth::user()->updateIp($request);
        $notificationsCount = NotificationModel::where('user', Auth::user()->id)->where('read', 0)->count();
        View::share('notificationCount', $notificationsCount);
        $mailDialogsOM = MailDialogModel::with('getDialogMessages')->where('user1', Auth::user()->id)->orWhere('user2', Auth::user()->id)->get();
        $mailsCount = 0;
        foreach($mailDialogsOM as $mM)
        {
            $mailsCount += $mM->getDialogMessages->where('sender', '<>', Auth::user()->id)->where('read', 0)->count();
        }
        View::share('mailCount', $mailsCount);
        $onlineUsers = UserModel::where('online_date', '>', time())->count();
        View::share('onlineUsers', $onlineUsers);

        if(Auth::user()->news_read > 0)
        {
            $news = ForumTopicModel::with('posts')->find(Auth::user()->news_read);
            if($news != null) View::share('news', $news);
        }
        if(Auth::user()->isBanned() && Route::current()->uri != 'block')
        {
            return redirect('/block');
        }
        if(Auth::user()->issetGift()) View::share('giftInfo', json_decode(Auth::user()->gift));
            if(Auth::user()->param_summ != Auth::user()->param_summm)
        {
            $us = UserModel::find(Auth::user()->id);
            $us->param_summ = Auth::user()->param_summm;
            $us->save();
        }
        $actionInfo = GameSettingModel::where('key', 'action')->first();
        if(strpos(Route::current()->uri, 'admin') !== false && Route::current()->uri != 'access' && Route::current()->uri != 'access/yes' && session('admin') != 1)
        {
            session(['referer_url' => URL::current()]);
            return redirect('/access');
        }
        elseif(strpos(Route::current()->uri, 'admin') === false && session('admin') == 1 && Route::current()->uri != 'access' && Route::current()->uri != 'access/yes') session(['admin' => null, 'referer_url' => null]);
        if($actionInfo->value != '')
        {
            $actInfo = json_decode($actionInfo->value);
            if($actInfo->time <= time())
            {
                $actionInfo->value = '';
                $actionInfo->save();
            }
            View::share('action', 1);
        }
        if(Auth::user()->exp_boost_time <= time() && Auth::user()->exp_boost > 0) Auth::user()->clearExpBoost();
        $referalTournament = GameSettingModel::where('key', 'referal_tournament')->first();
        View::share('referalTourn', $referalTournament->value != '' ? 1 : 0);
        if($referalTournament->value != '')
        {
            $referalTournamentJson = json_decode($referalTournament->value);
            if($referalTournamentJson->time <= time())
            {
                $notifTournament = 'Вы победили в конкурсе рефералов!<br> Награда:<br>';
                $top = UserModel::orderBy('referal_count', 'DESC')->first();
                if(isset($referalTournamentJson->valmers))
                {
                    $top->valmers += $referalTournamentJson->valmers;
                    $notifTournament .= $referalTournamentJson->valmers.' вальмер<br>';
                }
                if(isset($referalTournamentJson->exp))
                {
                    $top->exp += $referalTournamentJson->exp;
                    $notifTournament .= $referalTournamentJson->exp.' монет<br>';
                }
                $top->save();
                if(isset($referalTournamentJson->card))
                {
                    CardUserModel::create([
                        'user' => $top->id,
                        'card' => $referalTournamentJson->card,
                        'status' => 'redeemed'
                    ]);
                    $notifTournament .= 'Новая карта';
                }
                NotificationModel::create([
                    'user' => $top->id,
                    'notification' => $notifTournament
                ]);
                $referalTournament->value = '';
                $referalTournament->save();
            }
        }
        return $next($request);
    }
}
