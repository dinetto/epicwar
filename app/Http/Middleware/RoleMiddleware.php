<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!Auth::check()) return redirect('/');
        if($role == 'only-admins')
        {
            if(in_array(Auth::user()->role, ['user', 'moder']))
            {
                return redirect('/')->with('error', 'Доступ запрещён!');
            }
        }
        elseif($role == 'moders')
        {
            if(Auth::user()->role == 'user')
            {
                return redirect('/')->with('error', 'Доступ запрещён!');
            }
        }
        return $next($request);
    }
}
