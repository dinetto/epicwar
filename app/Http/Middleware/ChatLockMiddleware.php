<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\GameSettingModel;
use Auth;

class ChatLockMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(GameSettingModel::where('key', 'chat_enable')->first()->value == 0 && !Auth::user()->isRole())
        {
            return back()->with('error', 'Чат закрыт администратором!');
        }
        return $next($request);
    }
}
