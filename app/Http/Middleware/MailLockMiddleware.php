<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\GameSettingModel;
use Auth;

class MailLockMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(GameSettingModel::where('key', 'mail_enable')->first()->value == 0 && !Auth::user()->isRole())
        {
            return back()->with('error', 'Почта закрыта администратором!');
        }
        return $next($request);
    }
}
