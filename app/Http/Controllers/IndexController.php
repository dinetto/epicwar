<?php

namespace App\Http\Controllers;

use App\Models\BlockedIPModel;
use App\Models\ReferalModel;
use Illuminate\Http\Request;
use Auth;
use App\Models\UserModel;

class IndexController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function login()
    {
        return view('login');
    }

    public function loginConfirm(Request $request)
    {
        $currentIP = $request->ip();
        if(BlockedIPModel::where('ip', $currentIP)->first() != null) return back()->with('error', 'Ваш IP-адрес заблокирован!');
        $messages = [
            'login.exists' => 'Игрока не существует!',
            'login.between' => 'Ник должно быть в пределах :min-:max символов!',
            'login.required' => 'Введите ник!',
            'password.required' => 'Введите пароль',
            'password.between' => 'Пароль должен быть в пределах :min-:max символов!'
        ];
        $this->validate($request, [
            'login' => 'string|exists:users,login|between:5,32|required',
            'password' => 'string|required|between:6,64'
        ], $messages);
        if(Auth::attempt(["login" => $request->input('login'), "password" => $request->input('password')], 1))
        {
            return redirect('/game')->with('ok', 'Успешный вход!');
        }
        else return redirect('/')->with('error', 'Неверное имя игрока или пароль!');
    }

    public function start(Request $request)
    {
        $currentIP = $request->ip();
        if(BlockedIPModel::where('ip', $currentIP)->first() != null) return back()->with('error', 'Ваш IP-адрес заблокирован!');
        $user = new UserModel;
        $user->login = "Игрок_".rand(1,10000);
        $user->password = bcrypt(str_random(16));
        $user->register_date = time();
        $user->online_date = time();
        $user->save();
        if(session()->has('ref'))
        {
            $ref = session('ref');
            $ipComp = UserModel::where('ip', $request->ip())->count();
            $referal = UserModel::find($ref);
            if($ipComp == 0 && $referal != null)
            {
                ReferalModel::create([
                    'user' => $ref,
                    'referal' => $user->id,
                    'ip' => $request->ip()
                ]);
                $user->referal = $ref;
                $user->save();
                $referal->referal_count++;
                $referal->save();
            }
        }
        Auth::loginUsingId($user->id);
        return redirect('/tutorial/1');
    }

    public function ads()
    {
        return view('layouts.ads');
    }
 }
