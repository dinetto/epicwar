<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\UserModel;
use Config;

class SettingController extends Controller
{
    public function index()
    {
        return view('game.settings.index');
    }

    public function setting($type)
    {
        if(!in_array($type, ['login', 'password', 'sex'])) return back();
        return view('game.settings.'.$type);
    }

    public function login(Request $request)
    {
        $messages = [
            'login.required' => 'Введите новое имя игрока!',
            'login.unique' => 'Игрок с таким именем уже существует!',
            'login.between' => 'Длина имени должна быть в пределах :min-:max символов!',
            'login.alpha_num' => 'Имя содержит недопустимые символы!',
            'agree.required' => 'Подтвердите смену имени игрока!',
        ];
        $this->validate($request, [
            'login' => 'required|string|unique:users,login|between:5,32|alpha_num',
            'agree' => 'required'
        ], $messages);
        $user = UserModel::find(Auth::user()->id);
        if($user->login_changes == 0) $user->login_changes = 1;
        else
        {
            if($user->valmers < Config::get('login_cost')) return redirect('/settings')->with('error', 'Недостаточно вальмер!');
            $user->valmers -= Config::get('login_cost');
        }
        $user->login = $request->login;
        $user->save();
        return redirect('/settings')->with('ok', 'Имя игрока изменено!');
    }

    public function password(Request $request)
    {
        $messages = [
            'password.required' => 'Введите новый пароль!',
            'password.confirmed' => 'Пароли не совпадают!',
            'password.between' => 'Длина пароля должна быть в пределах :min-:max символов!',
            'password.alpha_num' => 'Пароль содержит недопустимые символы!',
            'agree.required' => 'Подтвердите смену пароля!',
        ];
        $this->validate($request, [
            'password' => 'required|string|confirmed|between:6,64|alpha_num',
            'agree' => 'required'
        ], $messages);
        $user = UserModel::find(Auth::user()->id);
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect('/settings')->with('ok', 'Пароль изменён!');
    }

    public function sex()
    {
        $user = UserModel::find(Auth::user()->id);
        if(Auth::user()->sex_changes < 3) $user->sex_changes++;
        else
        {
            if($user->valmers < 500) return redirect('/settings')->with('error', 'Недостаточно вальмер!');
            $user->valmers -= Config::get('game.sex_cost');
        }
        $user->sex = $user->sex == 'm' ? 'w' : 'm';
        $user->save();
        return redirect('/settings')->with('ok', 'Пол изменён!');
    }
}
