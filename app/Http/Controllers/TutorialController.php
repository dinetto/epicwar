<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\BattleLogModel;
use Auth;
use Services;
use Mail;
use App\Mail\EmailConfirmation;
use Cache;

class TutorialController extends Controller
{
    public function tutorial1()
    {
        return view('tutorial.1');
    }

    public function tutorial1Confirm($sex)
    {
        if (!in_array($sex, ['m', 'w'])) return redirect('/tutorial/1');
        $user = UserModel::find(Auth::user()->id);
        $user->tutorial_stage = 2;
        $user->sex = $sex;
        $user->save();
        return redirect('/tutorial/2');
    }

    public function tutorial2()
    {
        return view('tutorial.2');
    }

    public function tutorial2Confirm($action)
    {
        if ($action == 'yes') {
            Services::setTutorialStage(3);
            return redirect('/tutorial/3');
        } else {
            UserModel::destroy(Auth::user()->id);
            Auth::logout();
            return redirect('/');
        }
    }

    public function tutorial3()
    {
        return view('tutorial.3');
    }

    public function tutorial3Confirm(Request $request)
    {
        $messages = [
            'login.required' => 'ааВаЕаДаИбаЕ аНаИаК!',
            'login.between' => 'ааЛаИаНаА аНаИаКаА аДаОаЛаЖаНаА аБббб аВ аПбаЕаДаЕаЛаАб :min-:max баИаМаВаОаЛаОаВ!',
            'login.alpha_num' => 'ааИаК аДаОаЛаЖаЕаН баОаДаЕбаЖаАбб баОаЛбаКаО аЛаАбаИаНбаКаИаЕ баИаМаВаОаЛб, аКаИбаИаЛаЛаИбб аИ баИббб!',
            'login.unique' => 'ааГбаОаК б баАаКаИаМ аНаИаКаОаМ баЖаЕ бббаЕббаВбаЕб!',
            'password.between' => 'ааЛаИаНаА аПаАбаОаЛб аДаОаЛаЖаНаА аБббб аВ аПбаЕаДаЕаЛаАб :min-:max баИаМаВаОаЛаОаВ!',
            'password.required' => 'ааВаЕаДаИбаЕ аПаАбаОаЛб!',
            'password.alpha_num' => 'ааАбаОаЛб баОаДаЕбаЖаИб аНаЕаДаОаПбббаИаМбаЕ баИаМаВаОаЛб!'
        ];



        $this->validate($request, [
            'login' => 'string|required|between:5,32|alpha_num|unique:users',
            'password' => 'string|between:6,64|required|alpha_num'
        ], $messages);





        $user = UserModel::find(Auth::user()->id);
        $user->login = $request->input('login');
        $user->password = bcrypt($request->input('password'));
        Services::setTutorialStage(4);
        $user->save();
        return redirect('/game');
    }


    public function tutorial5Confirm(Request $request)
    {
        $messages = [
            'email.required' => 'ааВаЕаДаИбаЕ баЛаЕаКббаОаНаНбб аПаОббб!',
            'email.email' => 'ааЕаДаОаПбббаИаМбаЙ баОбаМаАб баЛаЕаКббаОаНаНаОаЙ аПаОббб!',
            'email.unique' => 'ааГбаОаК б аДаАаНаНаОаЙ баЛаЕаКббаОаНаНаОаЙ аПаОббаОаЙ баЖаЕ бббаЕббаВбаЕб!'
        ];
        $this->validate($request, [
            'email' => 'email|required|unique:users,email'
        ], $messages);
        $user = UserModel::find(Auth::user()->id);
        $user->email = $request->input('email');
        $user->save();

    }
}

