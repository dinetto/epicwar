<?php

namespace App\Http\Controllers;

use App\Models\NotificationModel;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Auth;
use App\Models\SupportTicketModel;
use App\Models\SupportMessageModel;
use App\Models\UserModel;
use Cache;

class SupportController extends Controller
{
    private $types = [null, 'Ошибки', 'Предложения', 'Вопросы администрации', 'Вопросы по игре'];
    public function index()
    {
        $myTickets = SupportTicketModel::where('user', Auth::user()->id)->where('status', '<>', 'closed')->orderBy('updated_at', 'DESC')->paginate(10);
        return view('game.support.index', ['tickets' => $myTickets]);
    }

    public function newTicket()
    {
        return view('game.support.new');
    }

    public function newTicketConfirm(Request $request)
    {
        $antispam = Cache::get('support:'.Auth::user()->id.':antispam');
        if($antispam-time() > 0) return redirect('/support')->with('error', 'Слишком частая отправка запросов!');
        $messages = [
            'type.integer' => 'Ошибка!',
            'type.in' => 'Ошибка!',
            'type.required' => 'Не указана тема!',
            'name.string' => 'Ошибка!',
            'name.required' => 'Введите название!',
            'name.between' => 'Длина названия должна быть в пределах :min-:max символов!',
            'text.string' => 'Ошибка!',
            'text.required' => 'Введите описание проблемы!',
        ];
        $this->validate($request, [
            'type' => 'integer|in:1,2,3,4|required',
            'name' => 'string|required|between:6,32',
            'text' => 'string|required'
        ], $messages);
        Cache::put('support:'.Auth::user()->id.':antispam', (time()+600), 1);
        $ticket = SupportTicketModel::create([
            'user' => Auth::user()->id,
            'type' => $request->input('type'),
            'name' => $request->input('name')
        ]);
        SupportMessageModel::create([
            'ticket' => $ticket->id,
            'user' => Auth::user()->id,
            'text' => $request->input('text')
        ]);
        $moders = UserModel::where('role', '<>', 'user')->get();
        foreach($moders as $mod)
        {
            NotificationModel::create([
                'user' => $mod->id,
                'notification' => 'Новый запрос в тех.поддержку, раздел <a href="/admin/support/'.$request->input('type').'">"'.$this->types[$request->input('type')].'"</a>.',
                'read' => 0
            ]);
        }
        return redirect('/support/ticket/'.$ticket->id);
    }

    public function ticket($id)
    {
        $ticket = SupportTicketModel::with('getMessages.getUser', 'getUser')->findOrFail($id);
        if($ticket->user != Auth::user()->id && Auth::user()->role == 'user') return redirect('/support')->with('error', 'Доступ закрыт!');
        $messages = $ticket->getMessages()->orderBy('created_at', 'ASC')->paginate(10);
        if(Auth::user()->role != 'user' && $ticket->status == 'notread')
        {
            $ticket->status = 'read';
            $ticket->save();
        }
        $statuses = ['nonread' => 'Ожидает ответа администрации', 'read' => 'Прочитан', 'answered' => 'Отвечен администрацией', 'closed' => 'Закрыт'];
        return view('game.support.ticket', ['ticket' => $ticket, 'messages' => $messages, "types" => $this->types, "statuses" => $statuses]);
    }

    public function send($id, Request $request)
    {
        $messages = [
            'text.string' => 'Ошибка!',
            'text.required' => 'Введите текст!'
        ];
        $this->validate($request, [
            'text' => 'string|required'
        ], $messages);
        $ticket = SupportTicketModel::findOrFail($id);
        if($ticket->status == 'closed') return redirect('/support')->with('error', 'Запрос закрыт!');
        if($ticket->user != Auth::user()->id && Auth::user()->role == 'user') return redirect('/support')->with('error', 'Нет доступа!');
        if(Auth::user()->role != 'user')
        {
            $status = 'answered';
            NotificationModel::create([
                'user' => $ticket->user,
                'notification' => 'На запрос <a href="/support/ticket/'.$ticket->id.'">"'.$ticket->name.'"</a> был получен ответ от администратора.',
                'read' => 0
            ]);
        }
        else
        {
            $status = 'nonread';
            $admins = UserModel::where('role', '<>', 'user')->get();
            foreach($admins as $adm)
            {
                NotificationModel::create([
                    'user' => $adm->id,
                    'notification' => 'На запрос <a href="/support/ticket/'.$ticket->id.'">"'.$ticket->name.'"</a> был получен ответ от игрока.',
                    'read' => 0
                ]);
            }
        }
        SupportMessageModel::create([
            'ticket' => $ticket->id,
            'user' => Auth::user()->id,
            'text' => $request->input('text'),
        ]);
        $ticket->status = $status;
        $ticket->save();
        return back();
    }

    public function close($id)
    {
        $ticket = SupportTicketModel::findOrFail($id);
        if($ticket->status == 'closed') return redirect('/support')->with('error', 'Запрос закрыт!');
        if($ticket->user != Auth::user()->id && Auth::user()->role == 'user') return redirect('/support')->with('error', 'Нет доступа!');
        $ticket->status = 'closed';
        $ticket->save();
        return redirect('/support');
    }
}
