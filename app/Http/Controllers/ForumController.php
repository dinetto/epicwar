<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\UserModel;
use App\Models\ForumCategoryModel;
use App\Models\ForumPostModel;
use App\Models\ForumTopicModel;

class ForumController extends Controller
{
    public function index()
    {
        $categories = ForumCategoryModel::all();
        return view('game.forum.index', ['categories' => $categories]);
    }

    public function category($categoryId)
    {
        $category = ForumCategoryModel::findOrFail($categoryId);
        $topics = ForumTopicModel::where('category', $categoryId)->orderBy('updated_at', 'DESC')->paginate(10);
        return view('game.forum.category', ['topics' => $topics, 'category' => $category]);
    }


    public function categoryCreate()
    {
        if(!Auth::user()->isRole('admin')) return back()->with('error', 'Нет доступа!');
        return view('game.forum.categoryCreate');
    }

    public function categoryCreateConfirm(Request $request)
    {
        $messages = [
            'name.required' => 'Введите название раздела!'
        ];
        $this->validate($request, [
            'name' => 'required|string'
        ], $messages);
        $category = ForumCategoryModel::create([
            'name' => $request->name,
            'type' => 'category'
        ]);
        return redirect('/forum/cat'.$category->id);
    }

    public function topic($topicId)
    {
        $topic = ForumTopicModel::with('categoryInfo')->findOrFail($topicId);
        $posts = ForumPostModel::with('authorInfo')->where('topic', $topicId)->orderBy('created_at', 'ASC')->paginate(10);
        return view('game.forum.topic', ['topic' => $topic, 'posts' => $posts]);
    }

    public function topicDelete($topicId)
    {
        if(!Auth::user()->isRole()) return back()->with('error', 'Нет доступа!');
        if(ForumTopicModel::find($topicId) == null) return back()->with('error', 'Тема не найдена!');
        ForumPostModel::where('topic', $topicId)->delete();
        $returnCategory = ForumTopicModel::find($topicId)->category;
        ForumTopicModel::destroy($topicId);
        return redirect('/forum/cat'.$returnCategory)->with('ok', 'Тема удалена!');
    }

    public function topicCreate($categoryId)
    {
        if(ForumCategoryModel::find($categoryId)->type == 'news') return back();
        return view('game.forum.topicCreate', ['categoryId' => $categoryId]);
    }

    public function topicCreateConfirm($categoryId, Request $request)
    {
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы на форуме!');
        $messages = [
            'name.required' => 'Введите название темы!',
            'text.required' => 'Введите текст!'
        ];
        $this->validate($request, [
            'name' => 'required|string',
            'text' => 'required|string'
        ], $messages);
        $topic = ForumTopicModel::create([
            'name' => $request->name,
            'category' => $categoryId,
            'author' => Auth::user()->id
        ]);
        ForumPostModel::create([
            'topic' => $topic->id,
            'author' => Auth::user()->id,
            'text' => $request->text,
            'type' => 'starter'
        ]);
        return redirect('/forum/top'.$topic->id);
    }

    public function postCreate($topicId, Request $request)
    {
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы на форуме!');
        $topic = ForumTopicModel::findOrFail($topicId);
        $messages = [
            'text.required' => 'Введите комментарий!'
        ];
        $this->validate($request, [
            'text' => 'required|string'
        ], $messages);
        ForumPostModel::create([
            'topic' => $topicId,
            'author' => Auth::user()->id,
            'text' => $request->text,
        ]);
        if($topic->category == 1)
        {
            $newsList = explode(';', Auth::user()->post_points_news);
            if(!in_array($topic->id, $newsList))
            {
                $user = UserModel::find(Auth::user()->id);
                $user->post_points += 5;
                $newsList[] = $topic->id;
                $user->post_points_news = implode(';', $newsList);
                $user->save();
            }
        }
        return back()->with('ok', 'Комментарий добавлен!');
    }

    public function editPost($postId)
    {
        $post = ForumPostModel::findOrFail($postId);
        if($post->author != Auth::user()->id) return back();
        return view('game.forum.postEdit', ['post' => $post]);
    }

    public function editPostConfirm($postId, Request $request)
    {
        $messages = [
            'text.required' => 'Введите комментарий!'
        ];
        $this->validate($request, [
            'text' => 'required|string'
        ], $messages);
        $post = ForumPostModel::findOrFail($postId);
        $post->text = $request->text;
        $post->save();
        return redirect('/forum/top'.$post->topic)->with('ok', 'Комментарий отредактирован!');
    }

    public function postDelete($postId)
    {
        if(!Auth::user()->isRole()) return back()->with('error', 'Нет доступа!');
        $post = ForumPostModel::findOrFail($postId);
        $postTopic = $post->topic;
        if($post->type == 'starter') return back();
        $post->delete();
        return redirect('/forum/top'.$postTopic)->with('ok', 'Комментарий удалён!');
    }

    public function changeStatus($topicid)
    {
        $topic = ForumTopicModel::findOrFail($topicid);
        if($topic->author != Auth::user()->id) return back();
        $topic->status = $topic->status == 'open' ? 'close' : 'open';
        $topic->save();
        return back();
    }
}
