<?php

namespace App\Http\Controllers;

use App\Models\NotificationModel;
use Illuminate\Http\Request;
use App\Models\ChatModel;
use App\Models\UserModel;
use Auth;
use Cache;

class ChatController extends Controller
{
    public function index()
    {
        $messages = ChatModel::with('getSenderInfo', 'getGetterInfo')->orderBy('created_at', 'desc')->paginate(10);
        return view('game.chat.index', ['messages' => $messages]);
    }

    public function sendConfirm(Request $request)
    {
        if(time()-Auth::user()->register_date < 1200) return redirect('/chat');
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы в чате!');
        $antispam = Cache::get('chat:'.Auth::user()->id.':antispam');
        if($antispam-time() > 0) return redirect('/chat')->with('error', 'Слишком частая отправка!');
        $messages = [
            'message.required' => 'Введите сообщение!',
            'message.string' => 'Введите сообщение!'
        ];
        $this->validate($request, [
            'message' => 'string|required'
        ], $messages);
        ChatModel::create([
            'user' => Auth::user()->id,
            'reply' => 0,
            'message' => $request->input('message')
        ]);
        Cache::put('chat:'.Auth::user()->id.':antispam', (time()+5), 1);
        if(Auth::user()->post_points_time <= time())
        {
            $user = UserModel::find(Auth::user()->id);
            $user->post_points++;
            $user->post_points_time = time()+600;
            $user->save();
        }
        return redirect('/chat')->with('ok', 'Сообщение отправлено!');
    }

    public function sendWithReply($id)
    {
        $user = UserModel::find($id);
        if($id == Auth::user()->id) return redirect('/chat')->with('error', 'Нельзя отправлять сообщения самому себе!');
        return view('game.chat.reply', ["user" => $user]);
    }

    public function sendWithReplyConfirm(Request $request)
    {
        if(time()-Auth::user()->register_date < 1200) return redirect('/chat');
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы в чате!');
        $antispam = Cache::get('chat:'.Auth::user()->id.':antispam');
        if($antispam-time() > 0) return redirect('/chat')->with('error', 'Слишком частая отправка!');
        $messages = [
            'message.required' => 'Введите сообщение!',
            'message.string' => 'Введите сообщение!',
            'user.integer' => 'Некорректный пользователь!',
            'user.required' => 'Не указан отправитель!',
            'user.exists' => 'Игрок не найден!'
        ];
        $this->validate($request, [
            'message' => 'string|required',
            'user' => 'integer|required|exists:users,id'
        ], $messages);
        if($request->input('user') == Auth::user()->id) return redirect('/chat')->with('error', 'Нельзя отправлять сообщения самому себе!');
        ChatModel::create([
            'user' => Auth::user()->id,
            'reply' => $request->input('user'),
            'message' => $request->input('message')
        ]);
        $notify = 'Вам ответили в чате!<br><a href="/chat">Перейти в чат</a>';
        NotificationModel::create([
            'user' => $request->input('user'),
            'notification' => $notify
        ]);
        Cache::put('chat:'.Auth::user()->id.':antispam', (time()+5), 1);
        if(Auth::user()->post_points_time <= time())
        {
            $user = UserModel::find(Auth::user()->id);
            $user->post_points++;
            $user->post_points_time = time()+600;
            $user->save();
        }
        return redirect('/chat')->with('ok', 'Сообщение отправлено!');
    }

    public function del($id)
    {
        if(ChatModel::find($id)->count() == 0) return redirect('/chat');
        ChatModel::destroy($id);
        return redirect('/chat')->with('ok', 'Сообщение удалено!');
    }
}
