<?php

namespace App\Http\Controllers;

use App\Models\GameSettingModel;
use App\Models\MailListModel;
use Illuminate\Http\Request;
use Auth;
use App\Models\UserModel;
use Carbon\Carbon;
use Services;

class MainController extends Controller
{
    public function index()
    {
        return view('game.main');
    }

    public function quit()
    {
        Auth::logout();
        return redirect('/');
    }

    public function profile($id)
    {
        $user = UserModel::findOrFail($id);
        $issetFriendList = MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'friendlist')->count();
        $issetBlackList = MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'blacklist')->count();
        $deck = explode(';', $user->deck);
        return view('game.user.profile', ["user" => $user, "issetFL" => $issetFriendList, "issetBL" => $issetBlackList]);
    }


    public function online()
    {
        $users = UserModel::where('online_date', '>', time())->paginate(10);
        return view('game.online', ["users" => $users]);
    }

    public function rating()
    {
        $users = UserModel::orderBy('strength','health','armor', 'DESC')->take(100)->paginate(10);
        $myPosition = 0;
        return view('game.rating', ["users" => $users, "myPosition" => $myPosition]);
    }

    public function block()
    {
        if(!Auth::user()->isBanned()) return redirect('/');
        return view('game.block');
    }

    public function collectGift()
    {
        if(!Auth::user()->issetGift()) return back();
        $gift = json_decode(Auth::user()->gift);
        $user = UserModel::find(Auth::user()->id);
        if(isset($gift->gold)) $user->gold += $gift->gold;
        if(isset($gift->silver)) $user->silver += $gift->silver;
        if(isset($gift->exp)) $user->exp += $gift->exp;
        $user->gift = '';
        $user->save();
        return back()->with('ok', 'Подарок получен!');
    }

    public function action()
    {
        $action = GameSettingModel::where('key', 'action')->first();
        if($action->value == '') return back()->with('error', 'Акций нет!');
        $actionArr = json_decode($action->value);
        if($actionArr->action == 1) return view('game.actions.1', ['action' => $actionArr]);
        elseif($actionArr->action == 2)
        {
            $card = CardModel::find($actionArr->card);
            return view('game.actions.2', ['action' => $actionArr, 'card' => $card]);
        }
        elseif($actionArr->action == 3) return view('game.actions.3', ['action' => $actionArr]);
        elseif($actionArr->action == 4) return view('game.actions.4', ['action' => $actionArr]);
    }

    public function faq()
    {
        return view('game.faq');
    }

    public function postTournament()
    {
        $users = UserModel::where('post_points', '>', 0)->orderBy('post_points', 'DESC')->paginate(10);
        return view('game.postTournament', ['users' => $users]);
    }
}
