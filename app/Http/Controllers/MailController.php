<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\Models\MailDialogModel;
use App\Models\UserModel;
use App\Models\MailMessageModel;
use App\Models\MailListModel;
use App\Rules\DialogRule;
use App\Rules\EditMessageRule;
use Cache;

class MailController extends Controller
{
    public function index()
    {
        $dialogs = MailDialogModel::with(['getDialogMessages' => function($query) {
            $query->orderBy('created_at', 'DESC');
        }, 'getUser1Info', 'getUser2Info'])->where('user1', '=', Auth::user()->id)->orWhere('user2', '=', Auth::user()->id)->orderBy('updated_at', 'DESC')->paginate(10);
        return view('game.mail.index', ["dialogs" => $dialogs]);
    }

    public function dialog($id)
    {
        $dialog = MailDialogModel::with('getUser1Info', 'getUser2Info')->findOrFail($id);
        if($dialog->user1 != Auth::user()->id && $dialog->user2 != Auth::user()->id) return redirect('/mail');
        $nonReadedMessages = MailMessageModel::where('dialog', '=', $id)->where('read', '=', '0')->where('sender', '<>', Auth::user()->id)->count();
        if($nonReadedMessages > 0) MailMessageModel::where('dialog', '=', $id)->where('read', '=', '0')->where('sender', '!=', Auth::user()->id)->update(['read' => '1']);
        $messages = MailMessageModel::with('getSenderInfo')->where('dialog', '=', $id)->orderBy('created_at', 'desc')->paginate(10);
        return view('game.mail.dialog', ['dialog' => $dialog, 'messages' => $messages]);
    }

    public function send(Request $request)
    {
        $messages = [
            'message.required' => 'Введите сообщение!',
            'dialog.integer' => 'Ошибка!',
            'dialog.required' => 'Не указан диалог!'
        ];
        $this->validate($request, [
            'message' => 'string|required',
            'dialog' => ['integer', 'required', new DialogRule]
        ], $messages);
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы в почте!');
        $antispam = Cache::get('mail:'.Auth::user()->id.':antispam');
        if($antispam != null && $antispam-time() > 0) return redirect('/mail/dialog/'.$request->input('dialog'))->with('error', 'Слишком часто отправляете сообщения!');
        MailMessageModel::create([
            'dialog' => $request->input('dialog'),
            'sender' => Auth::user()->id,
            'message' => $request->input('message'),
            'read' => '0'
        ]);
        $dialog = MailDialogModel::find($request->input('dialog'));
        $dialog->updated_at = Carbon::now();
        $dialog->save();
        Cache::put('mail:'.Auth::user()->id.':antispam', (time()+15), 1);
        return redirect('/mail/dialog/'.$request->input('dialog'));
    }

    public function edit($id)
    {
        $message = MailMessageModel::with('getDialog')->findOrFail($id);
        if($message->sender != Auth::user()->id) return redirect('/mail');
        if($message->read == '1') return redirect('/mail');
        return view('game.mail.edit', ['message' => $message]);
    }

    public function editConfirm(Request $request)
    {
        $messages = [
            'message_text.required' => 'Введите сообщение!',
            'message_id.integer' => 'Ошибка!',
            'message_id.required' => 'Не указано сообщение!'
        ];
        $this->validate($request, [
            'message_text' => 'string|required',
            'message_id' => ['integer', 'required', new EditMessageRule]
        ], $messages);
        $message = MailMessageModel::with('getDialog')->find($request->input('message_id'));
        $message->message = $request->input('message_text');
        $message->created_at = Carbon::now();
        $message->save();
        return redirect('/mail/dialog/'.$message->getDialog->id)->with('ok', 'Сообщение отредактировано!');
    }

    public function newMessage($id)
    {
        if(UserModel::find($id) == null) return redirect('/mail')->with('error', 'Игрок не найден!');
        if(MailListModel::where('user1', '=', $id)->where('user2', '=', Auth::user()->id)->where('type', '=', 'blacklist')->count() > 0) return redirect('/mail')->with('error', 'Вы в чёрном списке!');
        return view('game.mail.new', ['user_id' => $id]);
    }

    public function newMessageConfirm(Request $request)
    {
        $messages = [
            'user.integer' => 'Ошибка!',
            'user.exists' => 'Игрок не найден!',
            'user.required' => 'Ошибка!',
            'message.string' => 'Введите сообщение!',
            'message.required' => 'Введите сообщение!'
        ];
        $this->validate($request, [
            "user" => 'integer|exists:users,id|required',
            "message" => 'string|required'
        ], $messages);
        $antispam = Cache::get('mail:'.Auth::user()->id.':antispam');
        if($antispam != null && $antispam-time() > 0) return redirect('/mail')->with('error', 'Слишком часто отправляете сообщения!');
        if(Auth::user()->isMuted()) return back()->with('error', 'Вы заблокированы в почте!');
        if(MailListModel::where('user1', '=', $request->input('user'))->where('user2', '=', Auth::user()->id)->where('type', '=', 'blacklist')->count() > 0) return redirect('/mail')->with('error', 'Вы в чёрном списке!');
        if($request->input('user') == Auth::user()->id) return redirect('/mail')->with('error', 'Нельзя отправлять сообщения самому себе!');
        $issetMailDialog = MailDialogModel::whereRaw('(user1 = '.Auth::user()->id.' AND user2 = '.$request->input('user').') OR (user1 = '.$request->input('user').' AND user2 = '.Auth::user()->id.')')->first();
        if($issetMailDialog == null)
        {
            $mailDialog = MailDialogModel::create([
                'user1' => Auth::user()->id,
                'user2' => $request->input('user')
            ]);
        }
        else
        {
            $dialog = MailDialogModel::whereRaw('(user1 = '.Auth::user()->id.' AND user2 = '.$request->input('user').') OR (user1 = '.$request->input('user').' AND user2 = '.Auth::user()->id.')')->first();
            $dialog->updated_at = Carbon::now();
            $dialog->save();
        }
        $message = MailMessageModel::create([
            'dialog' => $issetMailDialog != null ? $issetMailDialog->id : $mailDialog->id,
            'sender' => Auth::user()->id,
            'message' => $request->input('message'),
            'read' => '0'
        ]);
        Cache::put('mail:'.Auth::user()->id.':antispam', (time()+15), 1);
        return redirect('/mail/dialog/'.$message->dialog);
    }

    public function friendList()
    {
        $friends = MailListModel::with('getInfo')->where('user1', '=', Auth::user()->id)->where('type', 'friendlist')->paginate(15);
        return view('game.mail.friendlist', ['friends' => $friends]);
    }

    public function blackList()
    {
        $blacklist = MailListModel::with('getInfo')->where('user1', '=', Auth::user()->id)->where('type', 'blacklist')->paginate(15);
        return view('game.mail.blacklist', ['blacklist' => $blacklist]);
    }

    public function addFriendList($id)
    {
        if(UserModel::find($id) == null) return back()->with('error', 'Игрок не найден!');
        if($id == Auth::user()->id) return back()->with('error', 'Нельзя добавить самого себя в друзья!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'blacklist')->count() == 1) return back()->with('error', 'Игрок в чёрном списке!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'friendlist')->count() == 0)
        {
            $addFriend = new MailListModel;
            $addFriend->type = 'friendlist';
            $addFriend->user1 = Auth::user()->id;
            $addFriend->user2 = $id;
            $addFriend->save();
            return redirect('/mail/friendlist')->with('ok', 'Игрок добавлен в друзья!');
        }
        return back()->with('error', 'Игрок уже в друзьях!');
    }

    public function addBlackList($id)
    {
        if(UserModel::find($id) == null) return back()->with('error', 'Игрок не найден!');
        if($id == Auth::user()->id) return back()->with('error', 'Нельзя добавить самого себя в чёрный список!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'friendlist')->count() == 1) return back()->with('error', 'Игрок в друзьях!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'blacklist')->count() == 0)
        {
            $addFriend = new MailListModel;
            $addFriend->type = 'blacklist';
            $addFriend->user1 = Auth::user()->id;
            $addFriend->user2 = $id;
            $addFriend->save();
            return redirect('/mail/blacklist')->with('ok', 'Игрок добавлен в чёрный список!');
        }
        return back()->with('error', 'Игрок уже в чёрном списке!');
    }

    public function delFriendList($id)
    {
        if(UserModel::find($id) == null) return back()->with('error', 'Игрок не найден!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'friendlist')->count() == 0) return back()->with('error', 'Игрока нет в друзьях!');
        MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'friendlist')->delete();
        return back()->with('ok', 'Игрок удалён из друзей!');
    }

    public function delBlackList($id)
    {
        if(UserModel::find($id) == null) return back()->with('error', 'Игрок не найден!');
        if(MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'blacklist')->count() == 0) return back()->with('error', 'Игрока нет в чёрном списке!');
        MailListModel::where('user1', '=', Auth::user()->id)->where('user2', '=', $id)->where('type', '=', 'blacklist')->delete();
        return back()->with('ok', 'Игрок удалён из чёрного списка!');
    }
}
