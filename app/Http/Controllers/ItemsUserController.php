<?php

namespace App\Http\Controllers;

use App\Models\ItemUserModel;
use App\Models\UserModel;
use App\Services\Services;
use Auth;

use Illuminate\Http\Request;

class ItemsUserController extends Controller
{
    public function equipment($id)
    {
        $user = UserModel::findOrFail($id);
        $helmet = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'helmet')->first();
        $shoulder = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'shoulder')->first();
        $armor = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'armor')->first();
        $gloves = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'gloves')->first();
        $left_hand = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'left_hand')->first();
        $right_hand = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'right_hand')->first();
        $legs = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'legs')->first();
        $boots = ItemUserModel::where('user', $id)->where('equip', 'yes')->where('type', 'boots')->first();

        return view("game.user.equipment", ["user" => $user, "helmet" => $helmet,
            "shoulder" => $shoulder, "armor" => $armor, "gloves" => $gloves, "left_hand" => $left_hand, "right_hand" => $right_hand, "legs" => $legs
            , "boots" => $boots]);
    }

    public function view_item($id)
    {
        $view_item = ItemUserModel::findOrFail($id);
        return view("game.user.view_item", ["view_item" => $view_item]);
    }

    public function bag()
    {
        $items = ItemUserModel::all()->where('user', 1)->where('equip', 'no');
        return view("game.user.bag", ["items" => $items]);
    }

    public function putOnItem($id)
    {
        $item = ItemUserModel::findOrFail($id);


        $putOnItem = ItemUserModel::where('user', Auth::user()->id)->where('type', $item->type)->where('equip', 'yes')->first();
        if ($item->user != Auth::user()->id) return back()->with('error', 'Вещь не принадлежит вам !');
        if ($item->equip == 'yes')  return back()->with('error', 'Вещь уже одета');
        //Если у игрока есть одета вещь такого же типа
        if (isset($putOnItem)) {
            $putOnItem->equip = 'no';
            $putOnItem->save();
            Services::changeParametrs($putOnItem->strength, $putOnItem->health, $putOnItem->armor, "-");

        }
        $item->equip='yes';
        $item->save();

        Services::changeParametrs($item->strength, $item->health, $item->armor);


       return back()->with('ok', 'Вы успешно надели ' . $item->name);

    }
}
