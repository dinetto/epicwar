<?php

namespace App\Http\Controllers;

use App\Models\GameSettingModel;
use Illuminate\Http\Request;
use App\Models\ReferalModel;
use App\Models\UserModel;
use Auth;

class ReferalController extends Controller
{
    public function index()
    {
        $tournament = GameSettingModel::where('key', 'referal_tournament')->first()->value != '' ? 1 : 0;
        return view('game.referals.index', ['tournament' => $tournament]);
    }

    public function myReferals()
    {
        $referals = ReferalModel::with('getUserInfo')->where('user', Auth::user()->id)->paginate(10);
        return view('game.referals.my', ['referals' => $referals]);
    }

    public function referalsRating()
    {
        $setting = GameSettingModel::where('key', 'referal_tournament')->first();
        if($setting->value == '') return redirect('/')->with('error', 'Конкурс рефералов не активен!');
        $rating = UserModel::orderBy('referal_count', 'DESC')->paginate(10);
        return view('game.referals.rating', ['rating' => $rating, "setting" => json_decode($setting->value)]);
    }

    public function setReferal($referal)
    {
        session()->put('ref', $referal);
        return redirect('/');
    }
}
