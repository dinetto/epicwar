<?php

namespace App\Http\Controllers;

use App\Http\Requests\ActionValidateRequest;
use App\Http\Requests\CardValidationRequest;
use App\Http\Requests\EditCardValidationRequest;
use App\Models\BlockedIPModel;

use App\Models\ChatModel;
use App\Models\MailDialogModel;
use App\Models\MailListModel;
use App\Models\MailMessageModel;
use App\Models\NotificationModel;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\SupportTicketModel;
use App\Models\UserModel;
use App\Models\ForumTopicModel;
use App\Models\ForumPostModel;
use App\Models\GameSettingModel;
use App\Rules\BlockRule;
use URL;
use Cache;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->game_settings = ['mail_enable', 'chat_enable', 'register_enable'];
    }

    public function access()
    {
        return view('admin.access');
    }

    public function accessConfirm(Request $request)
    {
        if (session('admin') == 1) return redirect('/admin');
        $messages = [
            'password.required' => 'Введите пароль',
            'password.string' => 'Неверный формат!',
            'password.in' => 'Неверный пароль'
        ];
        $this->validate($request, [
            'password' => 'required|string|in:' . config('game.admin')
        ], $messages);
        session(['admin' => 1]);
        return redirect(session('referer_url'));
    }

    public function index()
    {
        $mailStatus = GameSettingModel::where('key', 'mail_enable')->first()->value;
        $chatStatus = GameSettingModel::where('key', 'chat_enable')->first()->value;
        $registerStatus = GameSettingModel::where('key', 'register_enable')->first()->value;
        return view('admin.index', ['mail' => $mailStatus, 'chat' => $chatStatus, 'register' => $registerStatus]);
    }

    public function supportTickets()
    {
        return view('admin.support.index');
    }

    public function supportTicketsType($type)
    {
        if (!in_array($type, [1, 2, 3, 4])) return back();
        $tickets = SupportTicketModel::where('type', $type)->orderBy('updated_at', 'DESC')->paginate(10);
        return view('admin.support.tickets', ["tickets" => $tickets]);
    }

    public function addNews()
    {
        return view('admin.addNews');
    }

    public function addNewsConfirm(Request $request)
    {
        $messages = [
            'name.required' => 'Введите название темы!',
            'text.required' => 'Введите текст!',
        ];
        $this->validate($request, [
            'name' => 'required|string',
            'text' => 'required|string'
        ], $messages);
        $topic = ForumTopicModel::create([
            'author' => Auth::user()->id,
            'name' => $request->name,
            'category' => 1,
            'status' => $request->close == 'on' ? 'close' : 'open'
        ]);
        ForumPostModel::create([
            'topic' => $topic->id,
            'author' => Auth::user()->id,
            'text' => $request->text,
            'type' => 'starter'
        ]);
        $users = UserModel::get();
        $notify = '<center>
        ' . $topic->name . '<br>
		Текст: ' . mb_strimwidth($request->text, 0, 50, '...') . '<br>
		<a href="/forum/top' . $topic->id . '">Перейти</a>
        </center>';
        foreach ($users as $us) {
            NotificationModel::create([
                'user' => $us->id,
                'notification' => $notify,
                'read' => 0
            ]);
        }
        return redirect('/forum/top' . $topic->id)->with('ok', 'Новость добавлена!');
    }

    public function changeSettingStatus($setting)
    {
        $setting .= '_enable';
        if (!in_array($setting, $this->game_settings)) return back();
        $settingInfo = GameSettingModel::where('key', $setting)->first();
        if ($settingInfo->value == 0) $settingInfo->value = 1;
        else $settingInfo->value = 0;
        $settingInfo->save();
        return back()->with('ok', 'Настройка сохранена!');
    }

    public function blockUser($id = null)
    {
        if ($id != null) {
            $info = UserModel::findOrFail($id);
            return view('admin.blockUser', ['user' => 1, 'info' => $info]);
        }
        return view('admin.blockUser', ['user' => 0]);
    }

    public function blockUserConfirm(Request $request)
    {
        $messages = [
            'login.required' => 'Введите имя игрока!',
            'login.string' => 'Ошибка!',
            'login.exists' => 'Игрока не существует!',
            'login.not_in' => 'Нельзя заблокировать самого себя!',
            'type.required' => 'Укажите тип блокировки!',
            'type.string' => 'Ошибка!',
            'type.in' => 'Неверный тип блокировки!',
            'time.required' => 'Введите время блокировки!',
            'time.integer' => 'Неверный формат времени блокировки!',
            '_time.required' => 'Ошибка!',
            '_time.integer' => 'Ошибка!',
            '_time.in' => 'Ошибка!',
            'reason.required' => 'Введите причину блокировки!',
            'reason.string' => 'Ошибка!',
        ];
        $this->validate($request, [
            'login' => ['required', 'string', 'exists:users,login', new BlockRule, 'not_in:' . Auth::user()->login],
            'type' => 'required|string|in:ban,mute',
            'time' => 'required|integer',
            '_time' => 'required|integer|in:1,60,3600,86400',
            'reason' => 'required|string'
        ], $messages);
        $user = UserModel::where('login', $request->login)->first();
        $user->block_type = $request->type;
        $user->block_time = time() + ($request->time * $request->_time);
        $user->block_reason = $request->reason;
        $user->save();
        return redirect('/admin')->with('ok', 'Игрок заблокирован!');
    }

    public function gift()
    {
        return view('admin.gift');
    }

    public function giftConfirm(Request $request)
    {
        $messages = [
            'id.integer' => 'Неверный формат ID!',
            'id.required' => 'Введите ID игрока!',
            'gold.integer' => 'Неверный формат кол-ва вальмер!',
            'gold.required' => 'Введите кол-во вальмер!',
            'silver.integer' => 'Неверный формат кол-ва алмазов!',
            'silver.required' => 'Введите кол-во алмазов!',
        ];
        $this->validate($request, [
            'id' => 'integer|required',
            'gold' => 'integer|required',
            'silver' => 'integer|required',
        ], $messages);
        $gift = [];
        if ($request->gold > 0) $gift['gold'] = $request->gold;
        if ($request->silver > 0) $gift['silver'] = $request->silver;
        if ($request->id <= 0) DB::table('users')->update(['gift' => json_encode($gift)]);
        else {
            $user = UserModel::find($request->id);
            if ($user == null) return back()->with('error', 'Игрок не найден!');
            $user->gift = json_encode($gift);
            $user->save();
        }
        return redirect('/admin')->with('ok', 'Подарок выдан!');
    }

    public function deleteUser()
    {
        return view('admin.deleteUser');
    }

    public function deleteUserConfirm(Request $request)
    {
        $messages = [
            'id.required' => 'Введите ID удаляемого игрока!',
            'id.integer' => 'Неверный формат ID!',
            'id.exists' => 'Игрока с данным ID не существует!',
            'id.not_in' => 'Нельзя удалить самого себя!',
            'agree.required' => 'Подтвердите удаление!'
        ];
        $this->validate($request, [
            'id' => 'required|integer|exists:users,id|not_in:' . Auth::user()->id,
            'agree' => 'required'
        ], $messages);
        CardUserModel::where('user', $request->id)->delete();
        ChatModel::where('user', $request->id)->delete();
        NotificationModel::where('user', $request->id)->delete();
        MarketCardModel::where('user', $request->id)->delete();
        $mailDialogs = MailDialogModel::where('user1', $request->id)->orWhere('user2', $request->id)->get();
        foreach ($mailDialogs as $mD) {
            MailMessageModel::where('dialog', $mD->id)->delete();
            MailDialogModel::destroy($mD->id);
        }
        $topics = ForumTopicModel::where('author', $request->id)->get();
        foreach ($topics as $t) {
            ForumPostModel::where('topic', $t->id)->delete();
            ForumTopicModel::destroy($t->id);
        }
        MailListModel::where('user1', $request->id)->orWhere('user2', $request->id)->delete();
        $arenaBattles = ArenaBattleModel::where('user1', $request->id)->orWhere('user2', $request->id)->get();
        foreach ($arenaBattles as $aB) {
            ArenaCardsModel::where('battle', $aB->id)->delete();
            ArenaBattleModel::destroy($aB->id);
        }
        UserModel::destroy($request->id);
        return redirect('/admin')->with('ok', 'Игрок удалён!');
    }






    public function action()
    {
        return view('admin.action');
    }

    public function actionConfirm($type, ActionValidateRequest $request)
    {
        $result = [
            'action' => $type,
            'time' => time() + $request->time * 3600
        ];
        if ($type == 1) $result['percent'] = $request->percent;
        elseif ($type == 2) {
            $result['summ'] = $request->summ;
        } elseif ($type == 3) {
            $result['percent'] = $request->percent;
            $result['summ'] = $request->summ;
        } elseif ($type == 4) $result['percent'] = $request->percent;
        GameSettingModel::where('key', 'action')->update(['value' => json_encode($result)]);
        DB::table('users')->update(['action_used' => 0]);
        return redirect('/admin')->with('ok', 'Акция включена!');
    }

    public function referalTournament()
    {
        $cards = CardModel::all();
        return view('admin.referal', ['cards' => $cards]);
    }

    public function referalTournamentConfirm(Request $request)
    {
        $messages = [
            'time.integer' => 'Неверный формат времени!',
            'time.required' => 'Введите время конкурса!',
            'valmers.integer' => 'Неверный формат кол-ва вальмер!',
            'valmers.required' => 'Введите кол-во вальмер!',
            'exp.integer' => 'Неверный формат кол-ва монет!',
            'exp.required' => 'Введите кол-во монет!',
            'card.integer' => 'Неверный формат карты!',
            'card.exists' => 'Карты не существует!'
        ];
        $this->validate($request, [
            'time' => 'integer|required',
            'valmers' => 'integer|required',
            'exp' => 'integer|required',
            'card' => 'integer|exists:cards,id'
        ], $messages);
        $tourn = [];
        if ($request->valmers > 0) $tourn['valmers'] = $request->valmers;
        if ($request->exp > 0) $tourn['exp'] = $request->exp;
        if ($request->card > 0) $tourn['card'] = $request->card;
        $tourn['time'] = time() + $request->time * 86400;
        GameSettingModel::where('key', 'referal_tournament')->update(['value' => json_encode($tourn)]);
        return redirect('/admin')->with('ok', 'Конкурс начат!');
    }

    public function blockList()
    {
        $blockedUsers = UserModel::where('block_time', '>=', time())->paginate(10);
        return view('admin.blockList', ['blocked' => $blockedUsers]);
    }

    public function delBlock($id)
    {
        $user = UserModel::findOrFail($id);
        $user->block_type = '';
        $user->block_time = 0;
        $user->block_reason = '';
        $user->save();
        return back()->with('ok', 'Блокировка снята!');
    }

    public function editBlock($id)
    {
        $user = UserModel::findOrFail($id);
        return view('admin.blockEdit', ['user' => $user]);
    }

    public function editBlockConfirm($id, Request $request)
    {
        $messages = [
            'type.required' => 'Укажите тип блокировки!',
            'type.string' => 'Ошибка!',
            'type.in' => 'Неверный тип блокировки!',
            'time.required' => 'Введите время блокировки!',
            'time.integer' => 'Неверный формат времени блокировки!',
            '_time.required' => 'Ошибка!',
            '_time.integer' => 'Ошибка!',
            '_time.in' => 'Ошибка!',
            'reason.required' => 'Введите причину блокировки!',
            'reason.string' => 'Ошибка!',
        ];
        $this->validate($request, [
            'type' => 'required|string|in:ban,mute',
            'time' => 'required|integer|min:0',
            '_time' => 'required|integer|in:1,60,3600,86400',
            'reason' => 'required|string'
        ], $messages);
        $user = UserModel::findOrFail($id);
        $user->block_type = $request->type;
        $user->block_time += ($request->time * $request->_time);
        $user->block_reason = $request->reason;
        $user->save();
        return redirect('/admin')->with('ok', 'Блокировка изменена!');
    }

    public function blockIP()
    {
        return view('admin.blockIP');
    }

    public function blockIPConfirm(Request $request)
    {
        $messages = [
            'ip.required' => 'Введите IP-адрес!',
            'ip.ip' => 'Некорректный формат IP-адреса!',
            'ip.unique' => 'IP-адрес уже заблокирован!'
        ];
        $this->validate($request, [
            'ip' => 'required|ip|unique:blocked_ip,ip'
        ], $messages);
        if ($request->input('ip') == $request->ip()) return back()->with('error', 'Нельзя заблокировать себя!');
        BlockedIPModel::create([
            'ip' => $request->input('ip')
        ]);
        return redirect('/admin')->with('ok', 'IP-адрес заблокирован!');
    }

    public function authorizeOtherAccount()
    {
        return view('admin.authorize');
    }

    public function authorizeOtherAccountConfirm(Request $request)
    {
        $messages = [
            'id.required' => 'Введите ID игрока!',
            'id.exists' => 'Игрок не найден!',
            'id.integer' => 'Неверный формат!'
        ];
        $this->validate($request, [
            'id' => 'required|integer|exists:users,id'
        ], $messages);
        Cache::put('auth:' . $request->id, Auth::user()->id, (time() + 86400), 1);
        Auth::loginUsingId($request->id);
        return redirect('/game');
    }

    public function returnToAccount()
    {
        $key = 'auth:' . Auth::user()->id;
        if (!Cache::has($key)) return back();
        $id = Cache::get($key);
        Auth::loginUsingId($id);
        Cache::forget($key);
        return redirect('/game');
    }
}
