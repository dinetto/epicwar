<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Services;
use Auth;
use App\Models\UserModel;

class ChestController extends Controller
{
    private $chestTypes, $chests;

    public function __construct()
    {
        $this->chestTypes = ['common', 'rare', 'legendary'];
        $this->chests =  config('chests');
    }

    public function index()
    {
        return view('game.chest.index');
    }

    public function open($chest)
    {
        if(!in_array($chest, $this->chestTypes)) return back()->with('error', 'Ошибка!');
        $chestName = $chest.'_chest';
        if(Auth::user()->$chestName > time()) return back()->with('error', 'Сундук закрыт!');
        $chance = Services::getChance();
        $rewardType = [];
        while(true)
        {
            $rewardType = $this->chests[$chest]['rewards'][array_rand($this->chests[$chest]['rewards'])];
            if($chance >= $rewardType['chance']['min'] && $chance <= $rewardType['chance']['max']) break;
        }
        if($rewardType['type'] == 'exp')
        {
            $count = rand($rewardType['count']['min'], $rewardType['count']['max']);            
	Services::addMoney($count, 0);
            $message = 'Получено: '.$count.' монет!';
        }
        elseif($rewardType['type'] == 'valmers')
        {
            $count = rand($rewardType['count']['min'], $rewardType['count']['max']);
            Services::addMoney(0, $count);
            $message = 'Получено: '.$count.' вальмер!';
        }
        elseif($rewardType['type'] == 'card')
        {
            $randomCard = CardModel::whereIn('level', $rewardType['level'])->whereNotIn('type', ['tutorial-common', 'tutorial-premium'])->inRandomOrder()->first();
            CardUserModel::create([
                'user' => Auth::user()->id,
                'card' => $randomCard->id,
                'learn_time' => time()+$randomCard->learn_time*60,
                'status' => 'redeemed'
            ]);
            $message = 'Получена карта '.$randomCard->level.' уровня \"'.$randomCard->name.'\"!';
        }
        UserModel::find(Auth::user()->id)->update([
            $chestName => time()+$this->chests[$chest]['time']
        ]);
        return back()->with('ok', $message);
    }
}
