<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use Auth;
use App\Models\GameSettingModel;
use Xsolla\SDK\API\XsollaClient;
use Xsolla\SDK\API\PaymentUI\TokenRequest;
use Xsolla\SDK\Webhook\WebhookServer;
use Xsolla\SDK\Webhook\Message\Message;
use Xsolla\SDK\Exception\Webhook\XsollaWebhookException;


class PaymentController extends Controller
{
    public function index()
    {
        $tokenRequest = new TokenRequest(config('payment.project_id'), (string) Auth::user()->id);
        $tokenRequest->setUserEmail(Auth::user()->email)
            ->setSandboxMode(config('payment.sandbox'))
            ->setUserName(Auth::user()->login);
        $tokenRequestArray = $tokenRequest->toArray();
        $tokenRequestArray['settings']['ui']['components']['virtual_currency']['custom_amount'] = true;
        $xsollaClient = XsollaClient::factory(array(
            'merchant_id' => config('payment.merchant_id'),
            'api_key' => config('payment.api_key_factory'),
        ));
        $response = $xsollaClient->createPaymentUIToken(['request' => $tokenRequestArray]);
        $token = $response['token'];
        return view('payment.index', ['token' => $token]);
    }

    public function result()
    {
        $callback = function (Message $message) {
            switch ($message->getNotificationType()) {
                case Message::USER_VALIDATION:
                    $user = UserModel::find((int) $message->getUserId());
                    if($user == null)
                        $this->responseCode(400, 'INVALID_USER');
                    break;
                case Message::PAYMENT:
                    $count = $message->getPurchase()['virtual_currency']['quantity'];
                    $userId = (int) $message->getUserId();
                    $user = UserModel::find($userId);
                    $user->valmers += $count;
                    $action = GameSettingModel::where('key', 'action')->first();
                    if($action->value != '')
                    {
                        $act = json_decode($action->value);
                        if($act->action == 1) $user->valmers += round($count*($act->percent/100));
                        elseif($act->action == 2 && $count >= $act->summ)
                        {
                            CardUserModel::create([
                                'user' => $userId,
                                'card' => $act->card,
                                'status' => 'redeemed'
                            ]);
                        }
                    }
                    if($user->ref != 0)
                    {
                        $referal = UserModel::find($user->ref);
                        $referal->valmers += round($count/2);
                        $referal->save();
                    }
                    $user->save();
                    $this->responseCode(204, 'PURCHASE_SUCCESS');
                    break;
                case Message::REFUND:
                    $this->responseCode(204, 'PURCHASE_CANCEL');
                    break;
                default:
                    $this->responseCode(204, 'PURCHASE_CANCEL');
            }
        };
        $webhookServer = WebhookServer::create($callback, config('payment.code'));
        $webhookServer->start();
    }

    public function responseCode($id, $code, $message = '')
    {
        header ("HTTP/1.1 $id");
        $json = array("error" => array("code" => $code, "message" => $message));
        echo json_encode($json);
        exit;
    }
}
