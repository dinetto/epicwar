<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\NotificationModel;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = NotificationModel::where('user', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(10);
        NotificationModel::where('user', Auth::user()->id)->where('read', 0)->update(['read' => 1]);
        return view('game.notifications', ["notifications" => $notifications]);
    }

    public function clear()
    {
        NotificationModel::where('user', Auth::user()->id)->delete();
        return back()->with('ok', 'Уведомления очищены!');
    }
}
