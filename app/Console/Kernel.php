<?php

namespace App\Console;

use App\Models\NotificationModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            NotificationModel::truncate();
            ChatModel::truncate();
            $usersTourn = UserModel::where('post_points', '>', 0)->orderBy('post_points', 'DESC')->take(3)->get();
            $counter = 1;
            foreach($usersTourn as $uss)
            {
                if($counter == 1) $reward = 15;
                elseif($counter == 2) $reward = 10;
                else $reward = 5;
                $uss->valmers += $reward;
                $uss->save();
                $message = 'Вы заняли '.$counter.' место в конкурсе! Награда: '.$reward.' вальмер!<br>Поздравляем!';
                NotificationModel::create([
                    'user' => $uss->id,
                    'notification' => $message,
                    'read' => 0
                ]);
                $counter++;
            }
            $postPointTrunc = UserModel::all();
            foreach($postPointTrunc as $ppt)
            {
                $ppt->post_points = 0;
                $ppt->save();
            }

        })->daily();
        $schedule->call(function() {
            $users = UserModel::all();
            foreach($users as $us)
            {
                if($us->campaign_stage == '2.2')
                {
                    $campaignTemp = json_decode($us->campaign_temp);
                    $attacks = $campaignTemp->village <= 4 ? 10 : 15;
                    if($campaignTemp->find == 1 && $campaignTemp->battle == 1 && $campaignTemp->attacks < $attacks) $campaignTemp->attacks++;
                    $us->campaign_temp = json_encode($campaignTemp);
                }
                if($us->mob_battles < 10) $us->mob_battles++;
                $us->save();
            }
        })->everyTenMinutes();
        $schedule->call(function()
        {
            $userss = UserModel::where('updated_at', '<', Carbon::now())->inRandomOrder()->take(rand(5,15))->get();
            foreach($userss as $uss)
            {
                $uss->online_date = time()+7200;
                $uss->save();
            }
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
