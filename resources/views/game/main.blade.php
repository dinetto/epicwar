@extends('layouts.main')

@section('content')
<style>
.list-group {
    margin-bottom: 0;
}
.list-group {
    padding-left: 0;
    margin-bottom: 20px;
}
.text-center {
    text-align: center;
}
.boxin {
    background-color: #222047;
    background: url(/images/bg-foot.jpg) no-repeat center;
    color: #C1C0D4;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    padding: 5px;
    margin-top: -5px;
    margin-bottom: -25px;
    word-wrap: break-word;
}
</style>

<div class="boxin">

		  		<center><table width="85%">
			<tbody><tr>
			  <td width="50%">
              <a href="/chests">
              	<img src="/images/main/sokr.png" width="75px" alt="">
				</a>
			  </td>
			  <td width="50%">
				    <a href="/rating">
				    	<img src="/images/main/people.png" width="75px" alt="">
				</a>
			  </td>
			  			  <td width="50%">
                 <a href="/chat">
                 	<img src="/images/main/obshenie.png" width="75px" alt="">
				</a>
			  </td>
			</tr>
		  </tbody></table></center></div><br><br>
		  <div class="box">
	    <div class="menu">
			<center><a href="/pay"> Покупка ресурсов </a></center>
		</div>
    <div class="foot-info">
		  <table>
			<tr>
			  <td class="inf">
				<a href="#">
						<img src="/images/gl.png" alt="mail"> {{ Services::viewNumber(Auth::user()->gold) }}
                </a>
			  </td>			 
			  <td class="inf-center">
				<a href="/pay">
				    <img src="/images/rb.png" alt="mail"> {{ Services::viewNumber(Auth::user()->silver) }}
                </a>					
			  </td>			

			</tr>
		  </table>
		</div></br>
		
	</div>
		<div class="foot-link text-center">
		  <table width="100%">
			<tr>
			  <td width="50%">
			    <a href="#">
				  <img src="/images/main.png" width="120px" alt="">
				</a>
			  </td>
			  <td width="50%">
			    <a href="/profile/{{ Auth::user()->id }}">
				  <img src="/images/user.png" width="120px" alt="">
				</a>
			  </td>
			</tr>
		  </table>
		</div>
@endsection