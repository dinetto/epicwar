@extends('layouts.main')

@section('content')
<style>
.medis{
    background: url(/images/bg-foot.jpg) no-repeat center;
    padding: 5px;
    border-radius: 5px;
    word-wrap: break-word;
    border: 1px solid #3d3b6e;
    box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -webkit-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -moz-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
}
</style>
    <div class="box">
        <div class="medis">
          <div class="media-body">
            <center><h4 class="media-heading">Сокровищница </h4></center>
            <center>
            В каждом сундуке находится награда. </br> Вы можете получить все что угодно, от монет до необычных карт.</br>
       </center>
          </br>            
              <p>
            <div class="cards">
                <table class="text-center">
                    <tr>
                        <td>
                            @if(Auth::user()->common_chest > time())
                                <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-off-1.png&quot;);margin-right: 0px;m;margin-left: 20px;width: 50px;height: 42px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div><big>{{ Services::getChestTime(Auth::user()->common_chest - time()) }}</big></div></div></div>
                                @else
                                                            <a href="/chests/common/open">
                                <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-1.png&quot;);margin-right: 0px;margin-left: 20px;width: 50px;height: 41px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div></div></div></div>
                            </a>
                            @endif
                            </td>
                                                        <td>
                                                            @if(Auth::user()->rare_chest > time())  
                                 <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-off-2.png&quot;);margin-right: 10px;margin-left: 15px;width: 50px;height: 40px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div><big>{{ Services::getChestTime(Auth::user()->rare_chest - time()) }}</big></div></div></div>
                                 @else
                                 <a href="/chests/rare/open">
                                 <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-2.png&quot;);margin-right:0px;margin-left: 20px;width: 50px;height: 42px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div></div></div></div>
                             </a>
                                 @endif
                            </td>
                                                        <td>
                                                            @if(Auth::user()->legendary_chest > time())
                                 <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-off-3.png&quot;);margin-right: -15px;width: 50px;height: 42px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div><big>{{ Services::getChestTime(Auth::user()->legendary_chest - time()) }}</big></div></div></div>
                                 @else
                                 <a href="/chests/legendary/open">
                                 <div class="skill ibl" style="background-image:url(&quot;/images/crope/crop-3.png&quot;); margin-right: -15px;width: 50px;height: 43px;"><div class="it_brd q3p sh8"><div class="it_stat1"><div class="fl"></div></div><div class="clb"></div><div class="it_stat2 cntr"><div class="it_stars"></div></div></div></div>
                             </a>
                             @endif
                            </td>
                        
                    </tr>
                </table>
            </br>
            </div>
            </div>
        </div>
        <div class="foot-info">
          <table>
            <tr>
              <td class="inf">
                <a href="#">
                    <img src="/images/gl.png" alt="mail"> {{ Services::viewNumber(Auth::user()->gold) }}
                </a>
              </td>          
              <td class="inf-center">
                <a href="#">
                    <img src="/images/rb.png" alt="mail"> {{ Services::viewNumber(Auth::user()->silver) }}
                </a>                    

            </tr>
          </table>
        </div>
        </p>
        </div>
@endsection