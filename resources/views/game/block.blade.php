@extends('layouts.main')

@section('content')
    <center>
        <font color="white">
            Вы заблокированы!<br>
            Причина: {{ Auth::user()->block_reason }}<br>
            Время разблокировки: {{ Carbon\Carbon::createFromTimestamp(Auth::user()->block_time)->format('d.m.y H:i:s') }}
        </font>
    </center>
@endsection