@extends('layouts.main')

@section('content')
<div class = "box">
    <center>
        <big>Название темы</big>
        <form action="/forum/cat{{ $categoryId }}/create/yes" method="post">
            {{ csrf_field() }}
            <input type="text" class="form-control vvod-text" name="name" required><br>
            Текст:<br>
            <textarea style="max-width:100%;" class="form-control vvod-text" rows="3" name="text" required></textarea><br>
            <input type="submit" class="btn btn-games2 btn-block" value="Создать">
        </form>
    </center>
</div>
@endsection