@extends('layouts.main')

@section('content')
    <script>
        function addBB(html)
        {
            var e = document.getElementById("comment");
            if (e != null)
            {
                if(html == 'color-red') text = '[color=red][/color]';
                else if(html == 'color-blue') text = '[color=blue][/color]';
                else if(html == 'color-green') text = '[color=green][/color]';
                else if(html == 'url') text = '[url=][/url]';
                else if(html == 'img') text = '[img][/img]';
                else if(html == 'left') text = '[left][/left]';
                else if(html == 'center') text = '[center][/center]';
                else if(html == 'right') text = '[right][/right]';
                e.value += ''+text+'';
                e.focus();
            }
        }
    </script>
    <div class="box">
            <center>Форум / {{ $topic->categoryInfo->name }} / {{ $topic->name }}</center>
            @foreach($posts as $post)

            <div class="media">
          <div class="media-body">
          <h4 class="media-heading">
              @if($post->authorInfo == null)
                  <a href="#">Удалён</a>
              @else
                  <a href="/profile/{{ $post->authorInfo->id }}">{{ $post->authorInfo->login }}</a>
              @endif
              <span class="time pull-right">{{ $post->created_at }}</span>
                </h4>
                    {!! Services::bbcodes($post->text, ($post->authorInfo == null ? null : $post->author)) !!}
                    @if($post->type != 'starter' && Auth::user()->isRole())
                        <a href="/forum/post{{ $post->id }}/delete"> (del) </a>
                    @endif
                    @if($post->author == Auth::user()->id)
                        <a href="/forum/edit{{ $post->id }}"> [ред.] </a>
                    @endif
          </div>
        </div>
                @if($loop->iteration == 1 && $loop->count > 1 && $posts->currentPage() == 1)
                    <br><center><font color="white">Комментарии:</font></center>
                @endif
            @endforeach
        </div>
    </div>
    @if($topic->status == 'open')
        @if(Auth::user()->role != 'user')
            <span id="panel">
                <center>
                    <a href="javascript:addBB('color-red')"><button><font color="red">R</font></button></a>
                    <a href="javascript:addBB('color-green')"><button><font color="green">G</font></button></a>
                    <a href="javascript:addBB('color-blue')"><button><font color="blue">B</font></button></a>
                    <font color="black">
                        <a href="javascript:addBB('url')"><button><font color="black">URL</font></button></a>
                        <a href="javascript:addBB('img')"><button><font color="black">IMG</font></button></a>
                        <a href="javascript:addBB('left')"><button><font color="black">L</font></button></a>
                        <a href="javascript:addBB('center')"><button><font color="black">C</font></button></a>
                        <a href="javascript:addBB('right')"><button><font color="black">R</font></button></a>
                    </font>
                </center>
            </span>
        @endif
        <div class="box">
            <div class="menu">
            	<center><font color = "#ccd724">У вас <b>{{ Services::viewNumber(Auth::user()->post_points) }}</b> очк. общения</font></center><br>
                <center>
                    <form action="/forum/top{{ $topic->id }}/post" method="post">
                        {{ csrf_field() }}
                        <textarea id="comment" name="text" class="form-control vvod-text" rows = "3"></textarea><br>
                        <input type="submit" class = "btn btn-games2 btn-block" value="Отправить">
                    </form>
                </center>
            </div>
    @else
            <div class="menu">
             
            </div>
    @endif
    {{ $posts->links() }}
    @if(Auth::user()->isRole())
           <center> <div class="menu">
                <a href="/forum/top{{ $topic->id }}/delete">Удалить тему</a>
        @if($topic->author == Auth::user()->id || Auth::user()->isRole())
            <a href="/forum/top{{ $topic->id }}/changeStatus">{{ $topic->status == 'open' ? 'Закрыть тему' : 'Открыть тему' }}</a>
        @endif
            </div></center>
        </div>
    @endif
@endsection