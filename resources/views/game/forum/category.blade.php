@extends('layouts.main')

@section('content')
    <div class="box">
	    <div class="menu">
            <center>Форум / {{ $category->name }}</center>
        </div>
    </br>
	    <div class="menu">
            @forelse($topics as $top)
                <a href="/forum/top{{ $top->id }}"> <img src="/images/chat.png" alt=""> {{ $top->name }}</a>
            @empty
                Нет тем
            @endforelse
        </div>
    </div>
    @if($category->type != 'news')
        <div class="box">
            <div class="menu">
                <center><a href="/forum/cat{{ $category->id }}/create">Создать тему</a></center>
            </div>
        </div>
    @endif
@endsection