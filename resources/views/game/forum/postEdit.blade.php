@extends('layouts.main')

@section('content')
<div class = "box">
    <center>
        <form action="/forum/edit{{ $post->id }}/yes" method="post">
            {{ csrf_field() }}
            Текст:<br>
            <textarea name="text" class="form-control vvod-text" rows = "3" required">{{ $post->text }}</textarea><br>
            <input type="submit" class = "btn btn-games2 btn-block" value="Редактировать">
        </form>
    </center>
</div>
@endsection