@extends('layouts.main')

@section('content')
    <div class="box">
	    <div class="menu">
            @foreach($categories as $cat)
                <a href="/forum/cat{{ $cat->id }}"><img src="images/chat.png" alt=""> {{ $cat->name }}</a>
            @endforeach
        </div>
    </div>
    @if(Auth::user()->isRole('admin'))
        <div class="box">
            <div class="menu">
                <center><a href="/forum/create">Создать раздел</a></center>
            </div>
        </div>
    @endif
@endsection