@extends('layouts.main')

@section('content')
<style>
.medis {
    background: url(/images/bg-foot.jpg) no-repeat center;
    padding: 5px;
    border-radius: 5px;
    word-wrap: break-word;
    border: 1px solid #3d3b6e;
    box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -webkit-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -moz-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
}
</style>
    <font color="white">
    	<div class="box">
<div class="medis">
        <center>
        
            Купи любое количество золота и получи <strong>+{{ $action->percent }}%</strong><br>
            <small>Время действия: {{ Services::getChestTime($action->time-time()) }}</small><br>
        </center>
    </font>
</div>
<br><center><a class="btn btn-games2 btn-block" href="/pay">Перейти к покупке</a></center>
</div>
@endsection