@extends('layouts.main')

@section('content')
<style>
.medis {
    background: url(/images/bg-foot.jpg) no-repeat center;
    padding: 5px;
    border-radius: 5px;
    word-wrap: break-word;
    border: 1px solid #3d3b6e;
    box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -webkit-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -moz-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
}
</style>
    <font color="white">
<div class="box">
<div class="medis">
        <center>
            <big>Карта в подарок</big> </br> Купи {{ $action->summ }} вальмер и получи</br> <strong><font color = "#FF8C00">{{ $card->name }} {{ $card->level }} уровня</font></strong></strong><br>
<small>Акцией можно воспользоваться только 1 раз за все время ее действия.</br>
            Время действия: {{ Services::getChestTime($action->time-time()) }}</small><br>
                        @if(Auth::user()->action_used == 1)
               <small> Вы уже получили карту по акции</small><br>
            @endif
        </center>
    </font>
</div>
<br><center><a class="btn btn-games2 btn-block" href="/pay">Перейти к покупке</a></center>
</div>
    </font>
@endsection