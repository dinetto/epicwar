@extends('layouts.main')

@section('content')
<style>
.medis {
    background: url(/images/bg-foot.jpg) no-repeat center;
    padding: 5px;
    border-radius: 5px;
    word-wrap: break-word;
    border: 1px solid #3d3b6e;
    box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -webkit-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
    -moz-box-shadow: 0px 3px 5px 0px rgba(23,22,52,1);
}
</style>
<div class="box">
	<font color="white">
<div class="medis">
	<div class="media-body">
        <center>
            Купи {{ $action->summ }} вальмер и получи<br> <strong>+{{ $action->percent }}% к получаемым монетам</strong><br>
            <small>Время действия: {{ Services::getChestTime($action->time-time()) }}</small><br>
        </center>
    </font>
</div></div>
<br><center><a class="btn btn-games2 btn-block" href="/pay">Перейти к покупке</a></center>
</div>
    </font>
@endsection