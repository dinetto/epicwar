@extends('layouts.main')

@section('content')
    <div class="box">
        <center>
            <form action="{{ route('support.create') }}" method="post">
                {{ csrf_field() }}
                <center><big>Тема:</big><br></br>
                <select name="type" class="form-control vvod-text" required>
                    <option value="1">Ошибки</option>
                    <option value="2">Предложения</option>
                    <option value="3">Вопросы администрации</option>
                    <option value="4">Помощь по игре</option>
                </select><br>
                <big>Название:</big><br></br>
                <input style="max-width:100%;" type="text" class="form-control vvod-text" name="name" required><br>
                <big>Текст обращения:</big><br></br>
                <textarea style="max-width:100%;" name="text" class="form-control vvod-text" rows = "3" required></textarea><br>
                <input type="submit" class="btn btn-games2 btn-block" value="Создать">
            </form>
        </center>
    </div>
@endsection