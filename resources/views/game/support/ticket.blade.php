@extends('layouts.main')

@section('content')
<style>
.media a img {
    width: 45px;
}
</style>
    <div class="box">
    	<div class="media">
		  <div class="media-body"><center>
    	            Тема: <strong>{{ $types[$ticket->type] }}</strong><br>
            Название: <strong>{{ $ticket->name }}</strong><br>
            Автор: <strong>{{ $ticket->getUser->login }}</strong><br>
            Статус: <strong>{{ $statuses[$ticket->status] }}</strong>
                	    </center>
		  </div>
		</div>
		<br>
    	        @if($ticket->status != 'closed')
                <center>
                    <form action="/support/ticket/{{ $ticket->id }}/send" method="post">
                        {{ csrf_field() }}
                        <textarea name="text" class="form-control vvod-text" rows="3" required></textarea><br>
                        <input type="submit" class="btn btn-games2 btn-block" value="Отправить">
                    </form>
                </center>
        @endif    
        @foreach($messages as $m)
            	<div class="media">
            		<a class="pull-left">
            			@if($m->getUser->role  == 'user')
			<img class="media-object" src="{{ $m->getUser->sex == 'm' ? asset('/images/mail-men.png') : asset('/images/mail-woman.png') }}" alt="person">
			@else
			<img class="media-object" src="/images/support-img.png" alt="person">
			@endif  
		  </a>
		  <div class="media-body">
			<h4 class="media-heading">{{ ($m->getUser->role != 'user' && $m->user != $ticket->user) ? 'Поддержка' : $m->getUser->login }} <span class="time pull-right">{{ $ticket->created_at }}</span></h4>
			{{ $m->text }}
		  </div>
		</div>
        @endforeach
        @if($ticket->status != 'closed')
            </br><div class="menu"><center>
        <a href="/support/ticket/{{ $ticket->id }}/close">Закрыть тему</a>
    </center></div>
        @endif
        {{ $messages->render() }}
    </div>
@endsection