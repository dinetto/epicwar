@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            @forelse($tickets as $t)
                <a href="/support/ticket/{{ $t->id }}"> {{ $t->name }}
                @if($t->status == 'answered')
                    <font color="red" size="5"> + </font>
                @endif
                </a>
            @empty
                <center>Нет запросов</center>
            @endforelse
            {{ $tickets->links() }}
        </div></br>
        <div class="menu">
            <center><a href="/support/new"> Создать новый запрос </a></center>
        </div>
    </div>
@endsection