@extends('layouts.main')

@section('content')
<div class="box">
            <center><div class="media">
          <div class="media-body">
            <h4 class="media-heading">Рейтинг игроков</h4>
            Список ТОП-100 игроков по сумме параметров.
          </div></div>
        </center>
    </br>
        <div class="menu">
            @forelse($users as $us)
                <a href="/profile/{{ $us->id }}">
                    <img src="{{ $us->role == 'moder' ? asset('images/md.png') : asset('images/min-us.png') }}" width="17px" alt="person">
                    {{ $us->login }}
                        <font color="#cdcdc9"><u>{{ $us->strength }}</u>
                    <u>{{ $us->health }}</u>
                            <u>{{ $us->armor }}</u>

                        </font>

                    <span style="float:right">
                        {{ (($users->currentPage()-1)*10)+$loop->iteration }} место&nbsp;&nbsp;&nbsp;
                    </span>
                </a>
            @empty
                <center><font color="white">Рейтинг пока пуст:(</font></center>
            @endforelse
        </div>
    </div></div>
    {{ $users->render() }}
@endsection