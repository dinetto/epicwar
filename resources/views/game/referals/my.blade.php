@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            @forelse($referals as $ref)
                <a href="/profile/{{ $ref->referal }}">{{ $ref->getUserInfo->login }}</a>
            @empty
                <center>
                    <font color="white">
                Нет приглашенных игроков
                    </font>
                </center>
            @endforelse
        </div>
    </div>
    {{ $referals->render() }}
@endsection