@extends('layouts.main')

@section('content')
    	<div class="box">
        @forelse($notifications as $n)
        <div class="media">
		  <div class="media-body">
			{!! $n->notification !!}
		  </div>
		</div>
        @empty
            <div class="media">
		  <center><div class="media-body">Нет уведомлений
		  			  </div></center>
		</div>
        @endif
        {{ $notifications->render() }}
    </div>
@endsection