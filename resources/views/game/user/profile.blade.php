@extends('layouts.main')

@section('content')
<style>
.cards table {
    width: 85%;
    margin: auto;
}
</style>
        <script type="text/javascript">
    function addSmiley(id, html) {
        var e = document.getElementById(id);
        if (e != null) {
            e.value += ' ' + html + ' ';
            e.focus();
        }
    }
    function showDetails() {
        var e = document.getElementById('details');
        if (e != null) {
            if (e.style.display == 'block') { e.style.display = 'none'; } 
            else { e.style.display = 'block'; }
        }
    }
</script>
<script type="text/javascript">
    function addSmiley(id, html) {
        var e = document.getElementById(id);
        if (e != null) {
            e.value += ' ' + html + ' ';
            e.focus();
        }
    }

</script>
	<p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
		<h4 class="text-center name-user">
		    @if($user->online_date > time())
				<img src="{{ asset("images/on.png") }}" alt="">
			@else
				<img src="{{ asset("images/off.png") }}" alt="">
			@endif
			{{ $user->login }}<br>
			<small>{{ $user->sex == 'm' ? 'мужчина' : 'женщина' }}</small><br>
		</h4>
<div class="box">
	    <p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
	<ul class="list-unstyled stat">

			<li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Сила  <span class="s">{{ $user->strength }}</span></li>
			<li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Здровье   <span class="s">{{ $user->health }}</span></li>
			<li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Броня  <span class="s">{{ $user->armor }}</span></li>

	</ul>
	<div class="menu">
	<a href="/profile/equipment/{{ $user->id }}"> Снаряжение </a>
	</div>

	<p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>

<a onclick="{ showDetails(); return false; }" href="#details"><h4 class="text-center z">Открыть статистику</h4></a>
<div class="details" id="details" style="display: none;">
		<p>
		    <ul class="list-unstyled stat">


			  @if(Auth::user()->role != 'user')
				  <img src="{{ asset('images/b.png') }}" width="20px" alt=""> IP-адрес: <span class="s">{{ $user->ip }}</span>
			@endif
			</ul>
		</p>
		<p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
</div></ul>
		<div class="menu">
			@if($user->block_time < time() && Auth::user()->role != 'user' && $user->role == 'user')
				<center><a href="/admin/blockUser/{{ $user->id }}"> Выдать блокировку</a></center>
			@endif
			@if($user->id != Auth::user()->id)
				<center><a href="/mail/new/{{ $user->id }}"> Написать сообщение</a></center>

				@if($issetFL == 0 && $issetBL == 0)
			</div><br>
			<center>
<table width="50%">
			<tbody><tr>
			  <td width="70%">
              <a href="/mail/friendlist/add{{ $user->id }}">
              	<img src="/images/vdr.png" width="50px" alt="">
				</a>
			  </td>
			  <td width="30%">
				    <a href="/mail/blacklist/add{{ $user->id }}">
				    	<img src="/images/vchs.png" width="50px" alt="">
				</a>
			  </td>		
			</tr>
		  </tbody></table></center>
				@endif
			@else
				<a href="/bag"> Сумка 	</a>
				<a href="/settings"> Настройки</a>
				<a href="/referal"> Реферальная программа</a>
				<center><a href="/quit"> Выход</a></center>
			@endif
		</div></div></a>
@endsection