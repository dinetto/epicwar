@extends('layouts.main')

@section('content')

    <div class="box">
        <p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
        <ul class="list-unstyled stat">

            @forelse($items as $item)
            <div class="menu center-block">
                {{ $item->name  }} , {{\App\Services\Services::qualityItem($item->quality)}}
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Сила  <span class="s">{{ $item->strength }}</span></li>
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Здровье   <span class="s">{{ $item->health }}</span></li>
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Броня  <span class="s">{{ $item->armor }}</span></li>
                <a href="/bag/put_on/{{ $item->id }}"> Надеть </a>

            </div>
                <br>
            @empty
            <font color="white">Сумка пуста</font>
            @endforelse

            </ul>

        <p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
        </ul>

    </div>
@endsection