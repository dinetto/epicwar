@extends('layouts.main')

@section('content')

    <div class="box">
        <p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
        <ul class="list-unstyled stat">
            @if(!$helmet)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Голова <span class="s"></span></li>
            @else
                <a href="/view_item/{{$helmet->id}}">
                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$helmet->name}}
                        , {{\App\Services\Services::qualityItem($helmet->quality)}} <span
                                class="s"></span>
                    </li>
                </a>
            @endif
            @if(!$shoulder)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Наплечники <span class="s"></span></li>
            @else
                <a href="/view_item/{{$shoulder->id}}">

                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$shoulder->name}}
                        , {{\App\Services\Services::qualityItem($shoulder->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif


            @if(!$armor)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Броня <span class="s"></span></li>
            @else
                <a href="/view_item/{{$armor->id}}">
                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$armor->name}}
                        , {{\App\Services\Services::qualityItem($armor->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif

            @if(!$gloves)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Перчатки <span class="s"></span></li>
            @else
                <a href="/view_item/{{$gloves->id}}">
                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$gloves->name}}
                        , {{\App\Services\Services::qualityItem($gloves->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif

            @if(!$left_hand)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Левая рука <span class="s"></span></li>
            @else
                <a href="/view_item/{{$left_hand->id}}">
                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$left_hand->name}}
                        , {{\App\Services\Services::qualityItem($left_hand->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif

            @if(!$right_hand)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Правая рука <span class="s"></span></li>
            @else
                <a href="/view_item/{{$right_hand->id}}">
                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$right_hand->name}}
                        , {{\App\Services\Services::qualityItem($helmet->quality)}}<span
                                class="s"></span></li>
                </a>
            @endif

            @if(!$legs)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Ноги <span class="s"></span></li>
            @else
                <a href="/view_item/{{$legs->id}}">

                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$legs->name}}
                        , {{\App\Services\Services::qualityItem($legs->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif

            @if(!$boots)
                <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> Сапоги <span class="s"></span></li>
            @else
                <a href="/view_item/{{$boots->id}}">

                    <li><img src="{{ asset('images/b.png') }}" width="20px" alt=""> {{$boots->name}}
                        , {{\App\Services\Services::qualityItem($boots->quality)}} <span
                                class="s"></span></li>
                </a>
            @endif

        </ul>

        <p><img class="img-responsive" src="{{ asset('images/l.jpg') }}" alt=""></p>
        <a href="/profile/{{ $user->id }}"> ВЕРНУТЬСЯ НАЗАД </a>

    </div>
@endsection