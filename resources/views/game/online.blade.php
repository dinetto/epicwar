@extends('layouts.main')

@section('content')
    <div class="box">
            <center>
              <big>  Игроки онлайн</big><br><br>
            </center>
        <div class="menu">
            @foreach($users as $us)
                <a href="/profile/{{ $us->id }}">{{ $us->login }}</a>
            @endforeach
        </div>
    </div>
    {{ $users->render() }}
@endsection