@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            <center>
                Вы действительно хотите</br> сменить пол на <strong>{{ Auth::user()->sex == 'm' ? 'Женский' : 'Мужской' }}?</strong><br><br>
                <a href="/settings/sex/yes">Да</a>
                <a href="/settings">Нет</a></br>
                @if(Auth::user()->sex_changes < 3)
                    Бесплатных смен пола - {{ 3-Auth::user()->sex_changes }}.
                @else
                    Стоимость смены пола - {{ Config::get('game.sex_cost') }} вальмер.
                @endif
            </center>
        </div>
    </div>
@endsection