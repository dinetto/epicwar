@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            <center>
                <form action="{{ route('settings.password') }}" method="post">
                    {{ csrf_field() }}
                    Новый пароль:<br>
                    <input type="password" class="form-control vvod-text" rows="3" name="password" required><br>
                    Подтверждение пароля:<br>
                    <input type="password" class="form-control vvod-text" rows="3" name="password_confirmation" required>
                    <input type="checkbox" name="agree" required> Подтвердить смену пароля<br><br>
                    <input type="submit" class="btn btn-games2 btn-block" value="Сменить пароль">
                </form>
            </center>
        </div>
<br>
    <center><div class = "menu">
        <a href = "/settings"> Назад в настройки</a>
        </div></center>
@endsection