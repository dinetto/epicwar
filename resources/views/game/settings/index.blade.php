@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu"><a href="/settings/login">Смена имени игрока</a></div>
        <div class="menu"><a href="/settings/password">Смена пароля</a></div>
        <div class="menu"><a href="/settings/sex">Смена пола игрока</a></div>
    </div>
@endsection