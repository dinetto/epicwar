@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            <center>
                <form action="{{ route('settings.login') }}" method="post">
                    {{ csrf_field() }}
                    Новое имя игрока:<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="login" required>
                    <input type="checkbox" name="agree" required> Подтвердить смену<br><br>
                    <input type="submit" class="btn btn-games2 btn-block" value="Сменить имя">
                </form><br>
                @if(Auth::user()->login_changes < 1)
                    Бесплатных смен имени - 1.
                @else
                    Стоимость смены имени - {{ Config::get('game.login_cost') }} вальмер.
                @endif
            </center>
        </div><br>
    <center><div class = "menu">
        <a href = "/settings"> Назад в настройки</a>
        </div></center>
@endsection