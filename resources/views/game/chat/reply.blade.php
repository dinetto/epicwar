@extends('layouts.main')

@section('content')
 <script type="text/javascript">
    function addSmile(html) {
        var e = document.getElementById("message");
        if (e != null) {
            e.value += ' ' + html + ' ';
            e.focus();
        }
    }
    function showDetails() {
        var e = document.getElementById('details');
        if (e != null) {
            if (e.style.display == 'block') { e.style.display = 'none'; } 
            else { e.style.display = 'block'; }
        }
    }
</script>
<div class = "box">
    <center>
        @sendchat
            Ответ для: {{ $user->login }}<br>
             <center><a class="btn btn-games2 btn-block" onclick="{ showDetails(); return false; }" href="#details">Смайлы</a></center>
        <div class="details" id="details" style="display: none;">
            <center>
                <a href="javascript:addSmile(':grinning:')">@emojione(':grinning:')</a>
                <a href="javascript:addSmile(':joy:')">@emojione(':joy:')</a>
                <a href="javascript:addSmile(':relaxed:')">@emojione(':relaxed:')</a>
                <a href="javascript:addSmile(':innocent:')">@emojione(':innocent:')</a>
                <a href="javascript:addSmile(':relieved:')">@emojione(':relieved:')</a>
                <a href="javascript:addSmile(':flushed:')">@emojione(':flushed:')</a>
                <a href="javascript:addSmile(':dizzy_face:')">@emojione(':dizzy_face:')</a>
                <a href="javascript:addSmile(':mask:')">@emojione(':mask:')</a>
                <br>
                <a href="javascript:addSmile(':sunglasses:')">@emojione(':sunglasses:')</a>
                <a href="javascript:addSmile(':stuck_out_tongue_winking_eye:')">@emojione(':stuck_out_tongue_winking_eye:')</a>
                <a href="javascript:addSmile(':heart_eyes:')">@emojione(':heart_eyes:')</a>
                <a href="javascript:addSmile(':rage:')">@emojione(':rage:')</a>
                <a href="javascript:addSmile(':exploding_head:')">@emojione(':exploding_head:')</a>
                <a href="javascript:addSmile(':thermometer_face:')">@emojione(':thermometer_face:')</a>
                <a href="javascript:addSmile(':head_bandage:')">@emojione(':head_bandage:')</a>
                <a href="javascript:addSmile(':cowboy:')">@emojione(':cowboy:')</a>
                <br>
                <a href="javascript:addSmile(':poop:')">@emojione(':poop:')</a>
                <a href="javascript:addSmile(':rolling_eyes:')">@emojione(':rolling_eyes:')</a>
                <a href="javascript:addSmile(':persevere:')">@emojione(':persevere:')</a>
                <a href="javascript:addSmile(':scream:')">@emojione(':scream:')</a>
                <a href="javascript:addSmile(':grimacing:')">@emojione(':grimacing:')</a>
                <a href="javascript:addSmile(':shushing_face:')">@emojione(':shushing_face:')</a>
                <a href="javascript:addSmile(':face_with_hand_over_mouth:')">@emojione(':face_with_hand_over_mouth:')</a>
                <a href="javascript:addSmile(':face_with_monocle:')">@emojione(':face_with_monocle:')</a>
                <br>
                <a href="javascript:addSmile(':nerd:')">@emojione(':nerd:')</a>
                <a href="javascript:addSmile(':smiley_cat:')">@emojione(':smiley_cat:')</a>
                <a href="javascript:addSmile(':see_no_evil:')">@emojione(':see_no_evil:')</a>
                <a href="javascript:addSmile(':muscle:')">@emojione(':muscle:')</a>
                <a href="javascript:addSmile(':thumbsup:')">@emojione(':thumbsup:')</a>
                <a href="javascript:addSmile(':santa:')">@emojione(':santa:')</a>
                <a href="javascript:addSmile(':call_me:')">@emojione(':call_me:')</a>
                <a href="javascript:addSmile(':expressionless:')">@emojione(':expressionless:')</a>
                <br>
                <br>
           </center>
        </div>
            <form action="{{ route('chat.send.reply') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="user" value="{{ $user->id }}">
                <textarea id="message" name="message" class="form-control vvod-text" rows="3" required></textarea><br>
                <input type="submit" class="btn btn-games2 btn-block" value="Отправить">
            </form>
        @else
            Отправка невозможна!
        @endsendchat
    </center>
</div>
@endsection