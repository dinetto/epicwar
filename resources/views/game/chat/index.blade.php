@extends('layouts.main')

@section('content')
<style>
.btn-games2 {
    background: url(/images/btn-left2.jpg) no-repeat left ,url(/images/btn-right2.jpg) no-repeat right ,url(/images/btn-center2.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 55px;
    padding-right: 55px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 14px;
    margin: 0px;
    margin-bottom: 6px;
    border: 0;
}
</style>
 <script type="text/javascript">
    function addSmile(html) {
        var e = document.getElementById("message");
        if (e != null) {
            e.value += ' ' + html + ' ';
            e.focus();
        }
    }
    function showDetails() {
        var e = document.getElementById('details');
        if (e != null) {
            if (e.style.display == 'block') { e.style.display = 'none'; } 
            else { e.style.display = 'block'; }
        }
    }
</script>
    <div class="box">
        <center><a class="btn btn-games2 btn-block" href="?">Обновить ({{ Services::viewNumber(Auth::user()->post_points) }} очк.)</a></center></center>
    <center><a class="btn btn-games2 btn-block" onclick="{ showDetails(); return false; }" href="#details">Смайлы</a></center>
        <div class="details" id="details" style="display: none;">
            <center>
                <a href="javascript:addSmile(':grinning:')">@emojione(':grinning:')</a>
                <a href="javascript:addSmile(':joy:')">@emojione(':joy:')</a>
                <a href="javascript:addSmile(':relaxed:')">@emojione(':relaxed:')</a>
                <a href="javascript:addSmile(':innocent:')">@emojione(':innocent:')</a>
                <a href="javascript:addSmile(':relieved:')">@emojione(':relieved:')</a>
                <a href="javascript:addSmile(':flushed:')">@emojione(':flushed:')</a>
                <a href="javascript:addSmile(':dizzy_face:')">@emojione(':dizzy_face:')</a>
                <a href="javascript:addSmile(':mask:')">@emojione(':mask:')</a>
                <br>
                <a href="javascript:addSmile(':sunglasses:')">@emojione(':sunglasses:')</a>
                <a href="javascript:addSmile(':stuck_out_tongue_winking_eye:')">@emojione(':stuck_out_tongue_winking_eye:')</a>
                <a href="javascript:addSmile(':heart_eyes:')">@emojione(':heart_eyes:')</a>
                <a href="javascript:addSmile(':rage:')">@emojione(':rage:')</a>
                <a href="javascript:addSmile(':exploding_head:')">@emojione(':exploding_head:')</a>
                <a href="javascript:addSmile(':thermometer_face:')">@emojione(':thermometer_face:')</a>
                <a href="javascript:addSmile(':head_bandage:')">@emojione(':head_bandage:')</a>
                <a href="javascript:addSmile(':cowboy:')">@emojione(':cowboy:')</a>
                <br>
                <a href="javascript:addSmile(':poop:')">@emojione(':poop:')</a>
                <a href="javascript:addSmile(':rolling_eyes:')">@emojione(':rolling_eyes:')</a>
                <a href="javascript:addSmile(':persevere:')">@emojione(':persevere:')</a>
                <a href="javascript:addSmile(':scream:')">@emojione(':scream:')</a>
                <a href="javascript:addSmile(':grimacing:')">@emojione(':grimacing:')</a>
                <a href="javascript:addSmile(':shushing_face:')">@emojione(':shushing_face:')</a>
                <a href="javascript:addSmile(':face_with_hand_over_mouth:')">@emojione(':face_with_hand_over_mouth:')</a>
                <a href="javascript:addSmile(':face_with_monocle:')">@emojione(':face_with_monocle:')</a>
                <br>
                <a href="javascript:addSmile(':nerd:')">@emojione(':nerd:')</a>
                <a href="javascript:addSmile(':smiley_cat:')">@emojione(':smiley_cat:')</a>
                <a href="javascript:addSmile(':see_no_evil:')">@emojione(':see_no_evil:')</a>
                <a href="javascript:addSmile(':muscle:')">@emojione(':muscle:')</a>
                <a href="javascript:addSmile(':thumbsup:')">@emojione(':thumbsup:')</a>
                <a href="javascript:addSmile(':santa:')">@emojione(':santa:')</a>
                <a href="javascript:addSmile(':call_me:')">@emojione(':call_me:')</a>
                <a href="javascript:addSmile(':expressionless:')">@emojione(':expressionless:')</a>
                <br>
                <br>
           </center>
        </div>
        @sendchat
            <form action="{{ route('chat.send') }}" method="post">
                {{ csrf_field() }}
                <textarea id="message" style="max-width:100%;" name="message" class="form-control vvod-text" rows="3" required></textarea><br>
                <input type="submit" class="btn btn-games2 btn-block" value="Отправить">
            </form>
        @endsendchat
        <div class="chat-post">
        @forelse($messages as $m)
        <h5>
            <img src="{{ $m->getSenderInfo->role == 'moder' ? asset('images/md.png') : asset('images/min-us.png') }}" width="17px" alt="person">
             <a href = "/profile/{{ $m->getSenderInfo->id }}">
                {{ $m->getSenderInfo->login }}
            </a>
                <span class="time pull-right">{{ $m->created_at->diffForHumans() }}</span>
             @if(Auth::user()->id != $m->user)
             <span class="otvet"><a href="/chat/reply/{{ $m->user }}">[&raquo;]</a></span>
             @endif
                <br></h5>
            @if(Auth::user()->id == $m->reply)
            <div class="chat-post">
               <font color="#32CD32">
                   {{ $m->reply > 0 ? $m->getGetterInfo->login.', ' : '' }}
                   @emojione($m->message)
               </font>
</div>
            @else
                {{ $m->reply > 0 ? $m->getGetterInfo->login.', ' : '' }}
                @emojione($m->message)
            @endif
            @if(Auth::user()->role != 'user')
                &nbsp;<a href="/chat/del/{{ $m->id }}">[x]</a>
@if(Auth::user()->id != $m->user)
                 <a href="/admin/blockUser/{{ $m->getSenderInfo->id }}"> [Бан] </a>
                 @endif
            @endif
           </div>
           <div class="chat-post">
        @empty
            <center>Нет сообщений</center>
        @endforelse
        <center>Приятного общения</center></div>
        {{ $messages->links() }}
    </div>
@endsection