@extends('layouts.main')

@section('content')
    <div class="box">
            <center>
              <big>  Ежедневные награды за активность</big><br><br>
                <big>Ваши очки: {{ Auth::user()->post_points }}</big> <br><br>
            </center>
        <div class="menu">
            @forelse($users as $us)
                <a href="/profile/{{ $us->id }}">
                    {{ $us->login }} [{{ $us->post_points }}]
                    <span style="float:right">
                        {{ (($users->currentPage()-1)*10)+$loop->iteration }} место&nbsp;&nbsp;&nbsp;
                    </span>
                </a>
            @empty
                <center>
                    <font color="white">
                        Никто не участвует в получении награды!
                    </font>
                </center>
            @endforelse
        </div>
    </div>
    <div class="box">
        <center>
            <font color="white">
                Награда за 1 место - 15 вальмер.<br>
                Награда за 2 место - 10 вальмер.<br>
                Награда за 3 место - 5 вальмер.<br>
                ----<br>
                Сообщение в чате даёт 1 очко конкурса.<br>
                Сообщение в новости даёт 5 очков конкурса.<br>
                Результаты конкурса каждый день в 00:00.
            </font>
        </center>
    </div>
    {{ $users->render() }}
@endsection