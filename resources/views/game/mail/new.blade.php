@extends('layouts.main')

@section('content')
    <center><div class = "box">
       Cообщение для <strong>{{ Services::userInfo($user_id)->login }}</strong><br><br>
		  <p>
        <form action="{{ route('mail.new') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="user" value="{{ $user_id }}">
            <textarea class="form-control vvod-text" rows="3" name="message" required></textarea><br>
            <input class="btn btn-games2 btn-block" type="submit" value="Отправить">
        </form>
    </p></div>
    </center>
@endsection