@extends('layouts.main')

@section('content')
<style>
.media a img {
    width: 40px;
}
</style>
                <div class="box">
            @forelse($dialogs as $d)
<div class="media{{ $d->getDialogMessages->count() > 0 ? $d->getDialogMessages->first()->read == 0 && $d->getDialogMessages->first()->sender != Auth::user()->id ? ' otvet-mail' : null : null }}">
          <a class="pull-left" href="/mail/dialog/{{ $d->id }}">
            <img class="media-object" src="{{ $d->getUser1Info->sex == 'm' ? asset('images/mail-men.png') : asset('images/mail-woman.png') }}" alt="person">
          </a>
          <div class="media-body">
            <h4 class="media-heading">
                @if($d->getDialogMessages->count() > 0 && $d->getDialogMessages->first()->read == 0 && $d->getDialogMessages->first()->sender != Auth::user()->id)
                    <small>+{{ $d->getDialogMessages->where('read', 0)->where('sender', '<>', Auth::user()->id)->count() }}</small>
                @endif
                <a href="/mail/dialog/{{ $d->id }}">{{ $d->user1 == Auth::user()->id ? $d->getUser2Info->login : $d->getUser1Info->login }}</a>
            </h4>
            @if($d->getDialogMessages->count() > 0)
                 {{ mb_strimwidth($d->getDialogMessages->first()->message, 0, 20, '...') }}<br>
            @endif
          </div>
        </div>
        @empty
        <center>
        <div class="box">
          <div class="media">
          <div class="media-body">
          <h4 class="media-heading">
Нет диалогов
    </h4>
                  </div>
        </div></div></center>
        @endforelse
                                  <br><div class="menu">
            <center><a href="/mail/friendlist"> Список друзей</a></center>
        </div>
                <div class="menu">
            <center><a href="/mail/blacklist"> Черный список</a></center>
        </div>
        {{ $dialogs->links() }}

@endsection