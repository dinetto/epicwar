@extends('layouts.main')

@section('content')
<style>
.vvod {
    background: url(/images/vvod-left.jpg) no-repeat left ,url(/images/vvod-right.jpg) no-repeat right ,url(/images/vvod-center.jpg) repeat center;
    color: #c1c0d4;
    padding: 15px;
    border: none;
    text-align: center;
    font-size: 14px;
    box-shadow: none;
    border-top: none;
    border-bottom: none;
}

.input-lg {
    height: 40px;
    padding: 10px 16px;
    font-size: 14px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.btn-games2 {
    background: url(/images/btn-left2.jpg) no-repeat left ,url(/images/btn-right2.jpg) no-repeat right ,url(/images/btn-center2.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 55px;
    padding-right: 55px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 14px;
    margin: 0;
    margin-bottom: 0px;
    border: 0;
}
</style>
  <img class="img-responsive" src="/images/head.jpg" alt="head">
      </br><div class="box">
        <div class="media">
      <a class="pull-left" href="mail.html">
      <img class="media-object" src="/images/men.png" alt="...">
      </a>
      <div class="media-body">
      <h4 class="media-heading">Добродеятель </h4>
      Придумай что-нибудь более креативное, что бы другие игроки запоминали его, а так же с помощью своего логина ты сможешь <strong>зайти в игру</strong>.
      После ввода своего логина, тебе будет необходмо <strong>выбрать карту</strong>, с помощью которой ты <strong>проведёшь</strong> свой первый простой бой.
      </div>
    </div>
  </br>
        <form action="{{ route('tutorial3.confirm') }}" method="post">
            {{ csrf_field() }}
			  <div class="form-group">
            <input type="login" name="login" class="form-control vvod input-lg" id="exampleInputLogin1" placeholder="Логин" required>
              </div>
            <div class="form-group">

            <input type="login" name="password" class="form-control vvod input-lg" id="exampleInputLogin1" placeholder="Пароль" required>
        </div>
            <center><input type="submit" class="btn btn-games2" value="Продолжить"></center>
        </form></div>
  <img class="img-responsive" src="/images/new-foot.png" alt="foot-i">
@endsection