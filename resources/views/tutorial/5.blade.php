@extends('layouts.main')

@section('content')
<style>
.vvod {
    background: url(/images/vvod-left.jpg) no-repeat left ,url(/images/vvod-right.jpg) no-repeat right ,url(/images/vvod-center.jpg) repeat center;
    color: #c1c0d4;
    padding: 15px;
    border: none;
    text-align: center;
    font-size: 14px;
    box-shadow: none;
    border-top: none;
    border-bottom: none;
}

.input-lg {
    height: 40px;
    padding: 10px 16px;
    font-size: 14px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.btn-games2 {
    background: url(/images/btn-left2.jpg) no-repeat left ,url(/images/btn-right2.jpg) no-repeat right ,url(/images/btn-center2.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 55px;
    padding-right: 55px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 14px;
    margin: 0;
    margin-bottom: 0px;
    border: 0;
}
</style>
      <div class="box">
        <div class="media">
      <a class="pull-left">
      <img class="media-object" src="/images/men.png" alt="...">
      </a>
      <div class="media-body">
      <h4 class="media-heading">Добродеятель </h4>
    <strong>Запомни этот пароль</strong>, а если ты забудешь, то можешь легко <strong>восстановить</strong> его через электронную почту, <strong>введи адрес своей электронной почты</strong>, на которую мы будем отправлять ссылку для восстановления доступа.  
      </div>
    </div>
  </br>
    <center>
        <form action="{{ route('tutorial5.confirm') }}" method="post">
            {{ csrf_field() }}
            <input type="email" class="form-control vvod input-lg" id="exampleInputPassword1" name="email" required><br>
            <input type="submit" class="btn btn-games2 btn-block" value="Продолжить">
        </form>
    </center>
   </div>
     <img class="img-responsive" src="/images/new-foot.png" alt="foot-i">
@endsection