@extends('layouts.main')

@section('content')
		<style>
body {
    background-color: #222047;
    max-width: 350px;
    margin: auto;
    font-size: 13px;
    margin-top: -60px;
}
.vvod {
    background: url(/images/vvod-left.jpg) no-repeat left ,url(/images/vvod-right.jpg) no-repeat right ,url(/images/vvod-center.jpg) repeat center;
    color: #c1c0d4;
    padding: 15px;
    border: none;
    text-align: center;
    font-size: 14px;
    box-shadow: none;
    border-top: none;
    border-bottom: none;
}

.input-lg {
    height: 40px;
    padding: 10px 16px;
    font-size: 14px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.btn-games2 {
    background: url(/images/btn-left2.jpg) no-repeat left ,url(/images/btn-right2.jpg) no-repeat right ,url(/images/btn-center2.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 55px;
    padding-right: 55px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 14px;
    margin: 0;
    margin-bottom: 0px;
    border: 0;
}
.btn-games {
    background: url(/images/btn-left1.jpg) no-repeat left ,url(/images/btn-right1.jpg) no-repeat right ,url(/images/btn-center1.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    width: 210px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 16px;
    margin: 0;
    margin-bottom: 1px;
    border: 0;
}
	</style>
<img class="img-responsive" src="images/logo2.jpg" alt="LOGO">
<div class="box">
	<p class="text-center m-top">
		<a href="/start" class="btn btn-games">Начать игру</a>
	</p>
	<p class="text-center">
		Пренебрегай всеми правилами карточных игр и создай свою историю,
		в которой ты покоришь все вершины этой красивой легенды Властера.
	</p>
	<form role="form" action="{{ route('login.confirm') }}" method="post">
		{{ csrf_field() }}
		<div class="bg-form">
			<div class="form-group">
				<input type="login" name="login" class="form-control vvod input-lg" id="exampleInputLogin1" placeholder="Логин">
			</div>
			<div class="form-group">
				<input type="password" name="password" class="form-control vvod input-lg" id="exampleInputPassword1" placeholder="Пароль">
			</div>
		</div>
		<div class="text-center">
			<center><input type="submit" class="btn btn-games2 btn-block" value="Войти"></center>
		</div>
	</form>
</div>
<img class="img-responsive" src="/images/new-foot.png" alt="foot-i">
@endsection