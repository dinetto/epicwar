@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu">
            <div class="media">
    <center>
            <div class="media-body">
                    Вы получите <strong><img src="/images/rb.png" alt="mail"> {{ $summ }}0 вальмер</strong> + бонус</br>
                    К оплате <strong>{{ $summ }} руб.</strong> </br></br>
                    <center><big>Способы оплаты</big></br></br>

    <strong><strong>WebMoney</strong></br>

    <strong>QIWI</strong></br>

    <strong>YANDEX MONEY</strong></br>

    <strong>FREEKASSA</strong></br>

    <strong>INTERKASSA</strong></br>

    <strong>UNITPAY</strong></br>

    <strong>PAYEER</strong></br>
                </div></center></div></br>
                    <form action="https://wapkassa.ru/merchant/payment" method="post">
                        <input type="hidden" name="WK_PAYMENT_SITE" value="{{ config('game.payment.id') }}">
                        <input type="hidden" name="WK_PAYMENT_AMOUNT" value="{{ $summ }}.00">
                        <input type="hidden" name="WK_PAYMENT_COMM" value="{{ $comment }}">
                        <input type="hidden" name="WK_PAYMENT_HASH" value="{{ $hash }}">
                        <input type="hidden" name="WK_PAYMENT_USER" value="{{ Auth::user()->id }}">
                        <input type="submit" class="btn btn-games2 btn-block" value="Перейти к оплате"><br>
                </font>
            </center>
        </div>
    </div>
@endsection