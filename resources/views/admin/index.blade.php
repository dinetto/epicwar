@extends('layouts.main')

@section('content')
    <div class="box">
        <div class="menu"><a href="/admin/support">Управление тех.поддержкой</a></div>
        <div class="menu"><a href="/admin/blockUser">Заблокировать игрока</a></div>
        <div class="menu"><a href="/admin/blockList">Список блокировок</a></div>
        @if(Auth::user()->isRole('admin'))
            <div class="menu"><a href="/admin/gift">Выдать подарок</a></div>
            <div class="menu"><a href="/admin/authorize">Войти на аккаунт</a></div>
            <div class="menu"><a href="/admin/blockIP">Заблокировать IP адрес</a></div>
            <div class="menu"><a href="/admin/action">Запустить акцию</a></div>
            <div class="menu"><a href="/admin/referalTournament">Запустить конкурс рефералов</a></div>
            <div class="menu"><a href="/admin/deleteUser">Удалить игрока</a></div>
            <div class="menu"><a href="/admin/addNews">Добавить новость</a></div>
            <div class="menu"><a href="/admin/gameSetting/change/mail">{{ $mail == 0 ? 'Открыть' : 'Закрыть' }} доступ к почте</a></div>
            <div class="menu"><a href="/admin/gameSetting/change/chat">{{ $chat == 0 ? 'Открыть' : 'Закрыть' }} доступ к чату</a></div>
            <div class="menu"><a href="/admin/gameSetting/change/register">{{ $register == 0 ? 'Открыть' : 'Закрыть' }} доступ к регистрации</a></div>
        @endif
    </div>
@endsection