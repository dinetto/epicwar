@extends('layouts.main')

@section('content')
    <div class="box">
        @forelse($blocked as $b)
            <font color="white">
                Игрок: {{ $b->login }}<br>
                Тип блокировки: {{ $b->block_type == 'mute' ? 'Мут' : 'Бан' }}<br>
                До окончания: {{ Services::timer($b->block_time-time()) }}<br>
                Причина блокировки: {{ $b->block_reason }}<br>
                <a href="/admin/blockList/{{ $b->id }}/edit">Редактировать</a><br>
                <a href="/admin/blockList/{{ $b->id }}/del">Снять блокировку</a>
            </font><br>
            --------------------<br>
        @empty
            <div class="menu"><a disabled>Нет блокировок!</a></div>
        @endif
        {{ $blocked->render() }}
    </div>
@endsection