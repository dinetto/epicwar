@extends('layouts.main')

@section('content')
<div class = "box">
    <center>
        <form action="{{ route('admin.blockUser') }}" method="post">
            {{ csrf_field() }}
            Имя игрока:<br>
            <input type="text" class="form-control vvod-text" name="login" value="{{ $user == 0 ? null : $info->login }}" required><br>
            Тип блокировки:<br>
            <select name="type" class="form-control vvod-text" required>
                <option value="ban">Блокировка игры</option>
                <option value="mute">Блокировка чата, форума, почты</option>
            </select><br>
            Время блокировки:<br>
            <input type="text" class="form-control vvod-text" name="time" required><br>
            <select name="_time" class="form-control vvod-text"> 
                <option value="1">Секунд</option>
                <option value="60">Минут</option>
                <option value="3600">Часов</option>
                <option value="86400">Дней</option>
            </select><br>
            Причина блокировки:<br>
            <textarea name="reason" class="form-control vvod-text" required></textarea><br>
            <input type="submit" class="btn btn-games2 btn-block" value="Заблокировать">
        </form>
                            <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
    </center>
</div>
@endsection