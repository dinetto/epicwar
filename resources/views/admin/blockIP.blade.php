@extends('layouts.main')

@section('content')
<div class = "box">
    <center>
        <form action="{{ route('admin.blockIP') }}" method="post">
            {{ csrf_field() }}
            Блокируемый IP-адрес:<br>
            <textarea name="ip" class="form-control vvod-text" required></textarea><br>
            <input type="submit" class="btn btn-games2 btn-block" value="Заблокировать">
        </form>
                            <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
    </center>
</div>
@endsection