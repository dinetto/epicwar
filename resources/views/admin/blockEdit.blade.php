@extends('layouts.main')

@section('content')
<div class = "box">
    <center>
        <form action="/admin/blockList/{{ $user->id }}/edit/yes" method="post">
            {{ csrf_field() }}
            Тип блокировки:<br>
            <select name="type" class="form-control vvod-text" required>
                <option value="ban" {{ $user->block_type == 'ban' ? 'checked' : null }}>Блокировка игры</option>
                <option value="mute" {{ $user->block_type == 'mute' ? 'checked' : null }}>Блокировка чата, форума, почты</option>
            </select><br>
            Продлить время блокировки на:<br>
            <input type="text" class="form-control vvod-text" name="time" required><br>
            <select name="_time" class="form-control vvod-text">
                <option value="1">Секунд</option>
                <option value="60">Минут</option>
                <option value="3600">Часов</option>
                <option value="86400">Дней</option>
            </select><br>
            Причина блокировки:<br>
            <textarea name="reason" class="form-control vvod-text" required>{{ $user->block_reason }}</textarea><br>
            <input type="submit" class="btn btn-games2 btn-block" value="Изменить">
        </form>
                            <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
    </center>
</div>
@endsection