@extends('layouts.main')

@section('content')
    <script>
        function updateInput()
        {
            type = $("#type :selected").val();
            if(type == 1)
            {
                $("#percent").show();
                $("#percentInput").prop('required', true);
                $("#percentInput").prop('disabled', false);
                $("#summ").hide();
                $("#summInput").prop('required', false);
                $("#summInput").prop('disabled', true);
                $("#actionForm").prop('action', '/admin/action/1/yes');
            }
            else if(type == 2)
            {
                $("#percent").hide();
                $("#percentInput").prop('required', false);
                $("#percentInput").prop('disabled', true);
                $("#summ").show();
                $("#summInput").prop('required', true);
                $("#summInput").prop('disabled', false);
("#actionForm").prop('action', '/admin/action/2/yes');
            }
            else if(type == 3)
            {
                $("#percent").show();
                $("#percentInput").prop('required', true);
                $("#percentInput").prop('disabled', false);
                $("#summ").show();
                $("#summInput").prop('required', true);
                $("#summInput").prop('disabled', false);
                $("#actionForm").prop('action', '/admin/action/3/yes');
            }
            else if(type == 4)
            {
                $("#percent").show();
                $("#percentInput").prop('required', true);
                $("#percentInput").prop('disabled', false);
                $("#summ").hide();
                $("#summInput").prop('required', false);
                $("#summInput").prop('disabled', true);
                $("#actionForm").prop('action', '/admin/action/4/yes');
            }
            else
            {
                $("#percent").hide();
                $("#percentInput").prop('required', false);
                $("#percentInput").prop('disabled', true);
                $("#summ").hide();
                $("#summInput").prop('required', false);
                $("#summInput").prop('disabled', true);
                $("#actionForm").prop('action', '/admin/action/0/yes');
            }
        }

        setInterval('updateInput()', 1);
    </script>
    <div class = "box">
    <font color="orange">
        <center>
            <form action="" method="post" id="actionForm">
                {{ csrf_field() }}
                Тип акции:<br>
                <select name="type" class="form-control vvod-text" id="type" required>
                    <option value="0" selected></option>
                    <option value="1">% к покупке золоте</option>
                    <option value="3">% к получаемым серебру</option>
                    <option value="4">Скидка на карты</option>
                </select><br>
                <span id="percent" style="display: none;">
                    Процент:<br>
                    <input type="text" class="form-control vvod-text" name="percent" id="percentInput"><br>
                </span>
                <span id="summ" style="display: none;">
                    Сумма к пополнению:<br>
                    <input type="text" class="form-control vvod-text" name="summ" id="summInput"><br>
                </span>
                Время действия в часах:<br>
                <input type="text" class="form-control vvod-text" name="time" required><br>
                <input type="submit" class="btn btn-games2 btn-block" value="Запустить акцию">
            </form>
       
    </font>
                    <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
     </center>
</div>
@endsection