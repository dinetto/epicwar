@extends('layouts.main')

@section('content')
    <div class="box">
        <center>
            <font color="orange">
                <form action="{{ route('admin.deleteUser') }}" method="post">
                    {{ csrf_field() }}
                    ID игрока:<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="id" value="0" required>
                    <input type="checkbox" name="agree" required> Подтвердить удаление игрока<br><br>
                    <input type="submit" class="btn btn-games2 btn-block" value="Удалить игрока">
                </form>
            </font>
                <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
        </center>
    </div>
@endsection