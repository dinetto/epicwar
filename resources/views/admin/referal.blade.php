@extends('layouts.main')

@section('content')
    <div class="box">
        <center>
            <font color="orange">
                <form action="{{ route('admin.referalTournament') }}" method="post">
                    {{ csrf_field() }}
                    Время действия конкурса (в днях):<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="time" value="1" required><br>
                    Вальмеры:<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="valmers" value="0" required><br>
                    Монеты:<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="exp" value="0" required><br>
                    Карта:<br>
                    <select name="card" class="form-control vvod-text">
                        <option value="0" selected></option><br>
                        @foreach($cards as $c)
                            <option value="{{ $c->id }}">{{ $c->name }} [{{ $c->level }} ур.]</option>
                        @endforeach
                    </select><br>
                    <input type="submit" class="btn btn-games2 btn-block" value="Запустить конкурс">
                </form>
            </font>
                <div class = "menu">
        <a href = "/admin">Назад в панель </a>
    </div>
        </center>
    </div>
@endsection