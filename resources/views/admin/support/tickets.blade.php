@extends('layouts.main')

@section('content')
    <div class="box">
        @forelse($tickets as $t)
            <div class="menu"><a href="/support/ticket/{{ $t->id }}">{{ $t->name }}</a></div>
        @empty
            <div class="menu"><a disabled>Нет запросов!</a></div>
        @endif
        {{ $tickets->render() }}
    </div>
@endsection