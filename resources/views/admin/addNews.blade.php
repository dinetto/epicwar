@extends('layouts.main')

@section('content')
    <script>
        function addBB(html)
        {
            var e = document.getElementById("text");
            if (e != null)
            {
                if(html == 'color-red') text = '[color=red][/color]';
                else if(html == 'color-blue') text = '[color=blue][/color]';
                else if(html == 'color-green') text = '[color=green][/color]';
                else if(html == 'url') text = '[url=][/url]';
                else if(html == 'img') text = '[img][/img]';
                else if(html == 'left') text = '[left][/left]';
                else if(html == 'center') text = '[center][/center]';
                else if(html == 'right') text = '[right][/right]';
                e.value += ''+text+'';
                e.focus();
            }
        }
    </script>
        <span id="panel">
            <center>
                <a href="javascript:addBB('color-red')"><button><font color="red">R</font></button></a>
                <a href="javascript:addBB('color-green')"><button><font color="green">G</font></button></a>
                <a href="javascript:addBB('color-blue')"><button><font color="blue">B</font></button></a>
                <font color="black">
                    <a href="javascript:addBB('url')"><button><font color="black">URL</font></button></a>
                    <a href="javascript:addBB('img')"><button><font color="black">IMG</font></button></a>
                    <a href="javascript:addBB('left')"><button><font color="black">L</font></button></a>
                    <a href="javascript:addBB('center')"><button><font color="black">C</font></button></a>
                    <a href="javascript:addBB('right')"><button><font color="black">R</font></button></a>
                </font>
            </center>
        </span>
        <div class = "box">
            <center>
                <form action="{{ route('admin.addNews') }}" method="post">
                    {{ csrf_field() }}
                    Название новости:<br>
                    <input type="text" class="form-control vvod-text" rows="3" name="name" required><br>
                    Текст новости:<br>
                    <textarea id="text" name="text" class="form-control vvod-text" rows="3" required></textarea>
                    <input type="checkbox" name="close"> Запретить комментарии?<br><br>
                    <input type="submit" class="btn btn-games2 btn-block" value="Создать">
                </form>
                <div class = "menu">
                    <a href = "/admin">Назад в панель </a>
                </div>
            </center>
        </div>
@endsection