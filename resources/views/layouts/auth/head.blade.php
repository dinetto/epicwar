@auth
    @unbanned
		@if(Auth::user()->tutorial_stage >= 4)
		<style>
		body {
    background-color: #222047;
    max-width: 350px;
    margin: auto;
    font-size: 13px;
    margin-bottom: none;
}
.heads-info table tr td.inf {
    padding: 0px;
    text-align: center;
    font-size: 14px;
}
	</style>
			<img class="img-responsive" src="/images/head.jpg" alt="head">
			<div class="box">
				<div class="heads-info">
				  <table>
					<tr>
					  <td class="inf">
						<a href="/online">
							<img src="/images/users.png" alt="mail" width="26px">
							<span class="badge opoveshcheniye">{{ $onlineUsers }}</span>
						</a>
					  </td>
					  <td class="inf-center">
						<a href="/mail">
							<img src="/images/mail.png" alt="mail">
							@if($mailCount > 0)
								<span class="badge opoveshcheniye">{{ $mailCount }}</span>
							@endif
						</a>
					  </td>
					  <td class="inf">
						<a href="/notifications">
							<img src="/images/chat.png" alt="mail" width="38px">
							@if($notificationCount > 0)
								<span class="badge opoveshcheniye">{{ $notificationCount }}</span>
							@endif
						</a>
					  </td>
					</tr>
				  </table>
				</div>
			</div>
		@endif
		@muted
			<div class = "box">
				 <center><div class="media">
          <div class="media-body">
					Вы заблокированы в чате, почте и на форуме</br><small> Осталось {{ gmdate('d.m.y H:i:s', Auth::user()->block_time) }}<br>
					Причина: {{ Auth::user()->block_reason }}</small>
</div></div></center></div>
		@endmuted
		@gift
			<font color="white">
				<div class = "box">
				 <center><div class="media">
          <div class="media-body">
					<big>Подарок от Администрации</big><br>
					{!!  isset($giftInfo->gold) ? $giftInfo->gold.' серебро<br>' : '' !!}
					{!! isset($giftInfo->silver) ? $giftInfo->silver.' золото<br>' : '' !!}
				</div></div></center></br>
				<a href="/collectGift" class="btn btn-games2 btn-block">Забрать подарок</a></div>
			</font>
		@endgift
	@endunbanned
@endauth