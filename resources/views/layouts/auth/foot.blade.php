<style>
.mediss {
    box-shadow: 0px 0px 10px #ccd724, 0px 0px 10px #ccd724;
    border: 1px solid rgba(24,255,0,1);
    animation: glowPulse 2s infinite linear;
    -webkit-animation: glowPulse 2s infinite linear;
    background: url(/images/bg-head.jpg) no-repeat center;
    z-index: 10;
    padding: 5px;
    border-radius: 5px;
    word-wrap: break-word;
    border: 1px solid #3d3b6e;
}
.foot-link table {
    margin-top: 0px;
    margin: auto;
    margin-bottom: 5px;
    width: 90%;
}
</style>
@if(Auth::user()->tutorial_stage >= 4)
</div></div>
<div class="box">
    <center>
        @unbanned
            @if(Route::current()->uri != 'game')
                <div class="foot-link text-center">
              <table width="100%">
                <tr>
                  <td width="50%">
                    <a href="/game">
                      <img src="/images/main.png" width="120px" alt="">
                    </a>
                  </td>
                  <td width="50%">
                    <a href="/profile/{{ Auth::user()->id }}">
                      <img src="/images/user.png" width="120px" alt="">
                    </a>
                  </td>
                </tr>
              </table>
            </div><br>
            @endif
            @if(isset($action))
            <a href = "/action">
<div class="mediss">
                <big>Акция</big><br>
                Успей воспользоваться и получить преимущество
              </div>
            </a><br>
            @endif
            @if($referalTourn == 1)
                <a href="/referal/rating">Конкурс рефералов</a><br>
            @endif
            <a href="/forum"> Форум </a> -- 
            <a href="/support"> Тех.поддержка </a> --
            <a href="/faq"> Об игре </a>
            <br>
            @if(Auth::user()->role != 'user')
                <small><a href="/admin">Панель администратора</a></small><br>
            @endif
        @endunbanned
        <small>{{ \Carbon\Carbon::now()->format('d.m.y H:i:s') }}</small><br>
    </center>
    	</div></div>
<img class="img-responsive" src="/images/new-foot.png" alt="foot-i">
@endif