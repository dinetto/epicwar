<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="author" content="Plagsdev">
    <meta name="keywords" content="Легенды Властера, Plagsdev, плагсдев, онлайн игра, пвп, сражения, pvp, приключения, прокачка карт, карты, битва, турнир, арена, бесплатная, mmorpg">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/design.css') }}" />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/jquery.jrumble.1.3.min.js"></script>

        <title>Мобильная онлайн игра Легенды Властера</title>
    </head>
    <body>
        @auth
            @include('layouts.auth.head')
            @include('layouts.errors')
            @yield('content')
            @include('layouts.auth.foot')
        @else
            @include('layouts.guest.head')
            @include('layouts.errors')
            @yield('content')
            @include('layouts.guest.foot')
        @endauth
    </body>
</html>