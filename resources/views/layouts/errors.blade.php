@if (Session::has('ok'))
<center><div class="box">
          <div class="media">
          <div class="media-body">
          <h4 class="media-heading">
    {{Session::get('ok')}}
</h4>
          		  </div>
		</div></div></center>
@endif
@if (Session::has('error'))
<center><div class="box">
          <div class="media">
          <div class="media-body">
          <h4 class="media-heading">
    {{Session::get('error')}}
    </h4>
          		  </div>
		</div></div></center>
@endif
@if(isset($errors))
    @if (count($errors) > 0)
    <center><div class="box">
              <div class="media">
          <div class="media-body">
          <h4 class="media-heading">
          @foreach ($errors->all() as $error)
  {{ $error }} <br/>
      </h4>
          		  </div>
		</div></div></center>
          @endforeach
    @endif
@endif