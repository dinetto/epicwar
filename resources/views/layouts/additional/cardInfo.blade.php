<style>
.skill {
	width: 65px;
	height: 48px;
	background-repeat: no-repeat;
	background-position: 50% 50%;
	margin-bottom: 10px;
	margin-top: -3px;
	margin-left: 10px;
}

.ibl {
	display: inline-block;
}

.dummy {
	width: auto;
	text-align: center;
	margin-left: auto;
	margin-right: auto;
}
</style>
<table class="dummy">
	<tbody>
		<tr>
			<td class="1">
				<div class="skill ibl" style="background-image:url('/images/hp-user.png');margin-right:5px;">
					<div class="it_brd q3p sh8">
						<div class="it_stat1">
							<div class="fl"></div>
						</div>
						<div class="clb"></div>
						<div class="it_stat2 cntr" style="margin-top: 17px;">
							</div>{{ $health }}</div>
					</div>
				</div>
				</div>
				</div>
			</td>
			<td colspan="2" rowspan="4"><img src="/images/cards/{{ $image }}.png" alt="" width="65px"></td>
			<td class="1">
				<div class="slot">
					<div class="skill ibl" style="background-image:url('/images/defender-user.png');margin-right:5px;">
						<div class="it_brd q3p sh8">
							<div class="it_stat1">
								<div class="fl"></div>
							</div>
							<div class="clb"></div>
							<div class="it_stat2 cntr" style="margin-top: 17px;">
								</div>{{ $armor }}</div>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="1">
				<div class="slot">
					<div class="skill ibl" style="background-image:url('/images/str-user.png');margin-right:5px;">
						<div class="it_brd q3p sh8">
							<div class="it_stat1">
								<div class="fl"></div>
							</div>
							<div class="clb"></div>
							<div class="it_stat2 cntr" style="margin-top: 17px;">
								</div>{{ $attack }}</div>
						</div>
					</div>
				</div>
			</td>
			<td class="1">
				<div class="slot">
					<div class="skill ibl" style="background-image:url('/images/restore-user.png');margin-right:5px;">
						<div class="it_brd q3p sh8">
							<div class="it_stat1">
								<div class="fl"></div>
							</div>
							<div class="clb"></div>
							<div class="it_stat2 cntr" style="margin-top: 17px;">
								</div>{{ $cooldown }}c.</div>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="1">
		</tr>
	</tbody>
</table>
</div>
</td>