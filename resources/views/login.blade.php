@extends('layouts.main')

@section('content')
<style>
.input-lg {
    height: 40px;
    padding: 10px 16px;
    font-size: 14px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.btn-games2 {
    background: url(/images/btn-left2.jpg) no-repeat left ,url(/images/btn-right2.jpg) no-repeat right ,url(/images/btn-center2.jpg) repeat center;
    color: #ede256;
    text-align: center;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 55px;
    padding-right: 55px;
    text-shadow: 0 -1px 0 #131227, 0 -1px 0 #131227, 0 1px 0 #131227, 0 1px 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px 0 0 #131227, 1px 0 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227, -1px -1px 0 #131227, 1px -1px 0 #131227, -1px 1px 0 #131227, 1px 1px 0 #131227;
    font-size: 14px;
    margin: 0;
    margin-bottom: 0px;
    border: 0;
}
</style>
    <img class="img-responsive" src="/images/head.jpg" alt="head">
    <div class = "box"><center>
        <h3>Вход</h3>
        <form action="{{ route('login.confirm') }}" method="post">
            {{ csrf_field() }}
            <input type="login" class="form-control vvod input-lg" id="exampleInputLogin1" name="login" placeholder="Имя игрока" required><br>
            <input type="password" class="form-control vvod input-lg" id="exampleInputLogin1" name="password" placeholder="Пароль" required><br>
            <input type="submit" class="btn btn-games2 btn-block"  value="Войти">
        </form>
    </center></div>
    <img class="img-responsive" src="/images/new-foot.png" alt="foot-i">
@endsection