@if ($paginator->hasPages())
    <ul class="list-inline st mt text-center">
        <?php
            $start = $paginator->currentPage() - 1; // show 3 pagination links before current
            $end = $paginator->currentPage() + 1; // show 3 pagination links after current
            if($paginator->currentPage() > $paginator->total()) Redirect::to($paginator->url($paginator->lastPage()));
            if($start < 1) $start = 1; // reset start to 1
            if($start == 1 && $paginator->lastPage() > 3) $end = 3;
            if($paginator->currentPage() == $paginator->total() && $paginator->total() > 3) $start = $paginator->currentPage()-2;
            if($end >= $paginator->lastPage() ) $end = $paginator->lastPage(); // reset end to last page
        ?>
        @if (!$paginator->onFirstPage())
            <li><a href="{{ $paginator->url(1) }}" rel="prev">&laquo;</a></li>
        @endif
        @for ($i = $start; $i <= $end; $i++)
            @if($i == $paginator->currentPage())
                <li><a class="active" href="{{ $paginator->url($i) }}">{{$i}}</a></li>
            @else
                <li><a href="{{ $paginator->url($i) }}">{{$i}}</a></li>
            @endif
        @endfor
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->url($paginator->lastPage()) }}" rel="next">&raquo;</a></li>
        @endif
    </ul>
@endif