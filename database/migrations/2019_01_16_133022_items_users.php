<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemsUsers extends Migration
{

    public function up()
    {
        Schema::create('items_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_item');
            $table->string('name');
            $table->enum('quality',[1,2,3,4,5]);
            $table->enum('type', ['helmet', 'shoulder', 'armor','gloves','left_hand','right_hand','legs','boots']);
            $table->enum('equip', ['yes', 'no'])->default('no');
            $table->integer('strength');
            $table->integer('health');
            $table->integer('armor');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop("items_users");
    }
}
