<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login');
            $table->string('email')->default('');
            $table->string('password');
            $table->integer('register_date');
            $table->integer('online_date');
            $table->integer('tutorial_stage')->default('1');
            $table->integer('save')->default('0');
            $table->integer('email_confirmation')->default('0');
            $table->integer('exp')->default('0');
            $table->integer('gold')->default('0');
            $table->integer('silver')->default('0');
            $table->integer('strength')->default('25');
            $table->integer('health')->default('50');
            $table->integer('armor')->default('10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
