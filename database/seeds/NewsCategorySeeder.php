<?php

use Illuminate\Database\Seeder;
use App\Models\ForumCategoryModel;

class NewsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ForumCategoryModel::create([
            'name' => 'Новости',
            'type' => 'news'
        ]);
    }
}
