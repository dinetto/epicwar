<?php

use Illuminate\Database\Seeder;
use App\Models\GameSettingModel;

class GameSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
        GameSettingModel::create([
            'key' => 'chat_enable',
            'value' => 1
        ]);
        GameSettingModel::create([
            'key' => 'mail_enable',
            'value' => 1
        ]);
        GameSettingModel::create([
            'key' => 'register_enable',
            'value' => 1
        ]);
    }
}
