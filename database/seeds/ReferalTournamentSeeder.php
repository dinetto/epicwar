<?php

use Illuminate\Database\Seeder;
use App\Models\GameSettingModel;

class ReferalTournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GameSettingModel::create([
            'key' => 'referal_tournament',
            'value' => ''
        ]);
    }
}
