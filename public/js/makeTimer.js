var time = 0;
var div;
function addNull(number)
{
    var result;
    if(number >= 0 && number < 10) result = "0"+number;
    else result = number;
}
function makeTimer()
{
    var seconds, minutes, hours, timer, block;
    if(time >= 60)
    {
        hours = 0;
        minutes = Math.round(time/60);
        second = time - (minutes*60);
    }
    else if(time >= 3600)
    {
        hours = Math.round(time/3600);
        minutes = Math.round((time-hours*3600)/60);
        seconds = time - (hours*3600) - (minutes*60);
    }
    else
    {
        hours = 0;
        minutes = 0;
        seconds = time;
    }
    if(hours > 0) timer = addNull(hours)+"ч."+addNull(minutes)+"м."+addNull(seconds)+"с.";
    else if(hours == 0 && minutes > 0) timer = addNull(minutes)+"м."+addNull(seconds)+"c.";
    else timer = addNull(seconds)+"c.";
    if(timer > 0) time--;
    block = document.getElementById(div);
    block.html()
}