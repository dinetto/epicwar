<?php
return [
	'common' => [
		'rewards' => [
			[
				'chance' => [
					'min' => 3.1,
					'max' => 99
				],
				'count' => [
					'min' => 300,
					'max' => 500
				],
                'type' => 'exp'
			],
			[
				'chance' => [
					'min' => 0.1,
					'max' => 3
				],
				'count' => [
					'min' => 1,
					'max' => 5
				],
                'type' => 'valmers'
			],
			[
				'chance' => [
					'min' => 99.1,
					'max' => 100
				],
				'level' => [3, 4],
                'type' => 'card'
			]
		],
		'time' => 86400
	],
	'rare' => [
		'rewards' => [
			[
				'chance' => [
					'min' => 5.1,
					'max' => 99
				],
				'count' => [
					'min' => 500,
					'max' => 800
				],
                'type' => 'exp'
			],
			[
				'chance' => [
					'min' => 0.1,
					'max' => 5
				],
				'count' => [
					'min' => 5,
					'max' => 10
				],
                'type' => 'valmers'
			],
			[
				'chance' => [
					'min' => 99.1,
					'max' => 100
				],
				'level' => [5],
                'type' => 'card'
			]
		],
		'time' => 86400 * 3
	],
	'legendary' => [
		'rewards' => [
			[
				'chance' => [
					'min' => 14.6,
					'max' => 99
				],
				'count' => [
					'min' => 800,
					'max' => 1200
				],
                'type' => 'exp'
			],
			[
				'chance' => [
					'min' => 0.1,
					'max' => 5
				],
				'count' => [
					'min' => 3000,
					'max' => 5000
				],
                'type' => 'exp'
			],
			[
				'chance' => [
					'min' => 5.1,
					'max' => 10
				],
				'count' => [
					'min' => 20,
					'max' => 40
				],
                'type' => 'valmers'
			],
			[
				'chance' => [
					'min' => 10.1,
					'max' => 14.5
				],
				'count' => [
					'min' => 50,
					'max' => 100
				],
                'type' => 'valmers'
			],
			[
				'chance' => [
					'min' => 99.6,
					'max' => 100
				],
				'level' => [6],
                'type' => 'card'
			]
		],
		'time' => 86400 * 7
	]
];
