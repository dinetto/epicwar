<?php

return [
    'chapter1' => [
        'stage1' => [
            'type' => 'text',
            'text' => 'По легендам одного человека по имени Властер, лес, это самое подходящее место для разбойников, которые нападают на людей, твоя первоначальная задача - изгнать этих разбойников из этого леса и обеспечить спокойную жизнь людям.',
            'button' => 'Рейд (1 час)',
            'cooldown' => 60,
            'user' => [
                'complete' => 1
            ],
            'next' => '1.2',
        ],
        'stage2' => [
            'type' => 'raid',
            'cooldown' => 0,
            'enemies' => [
                null,
                [
                    'attack' => 15,
                    'health' => 90,
                    'armor' => 20,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 2,
                        'exp' => 400,
                        'campaign_exp' => 400
                    ]
                ],
                [
                    'attack' => 30,
                    'health' => 140,
                    'armor' => 40,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 6,
                        'exp' => 600,
                        'campaign_exp' => 500
                    ]
                ],
                [
                    'attack' => 50,
                    'health' => 190,
                    'armor' => 80,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 9,
                        'exp' => 2000,
                        'campaign_exp' => 1000
                    ]
                ],
                [
                    'attack' => 80,
                    'health' => 250,
                    'armor' => 130,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 18,
                        'exp' => 3200,
                        'campaign_exp' => 1500
                    ]
                ],
                [
                    'attack' => 150,
                    'health' => 400,
                    'armor' => 190,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 22,
                        'exp' => 5000,
                        'campaign_exp' => 2000
                    ]
                ],
                [
                    'attack' => 230,
                    'health' => 600,
                    'armor' => 240,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 30,
                        'exp' => 8000,
                        'campaign_exp' => 2500
                    ]
                ],
                [
                    'attack' => 400,
                    'health' => 900,
                    'armor' => 85,
                    'card' => rand(1,29),
                    'reward' => [
                        'valmers' => 5,
                        'exp' => 1500,
                        'campaign_exp' => 500
                    ]
                ],
            ],
            'text_if_success' => 'Теперь в этом лесу на одного разбойника меньше, но не стоит расслабляться. Вы так же можете встретить учеников самого Властера, которые выбрали не ту сторону. Продолжайте рейд.',
            'text_if_fail' => 'Вы потерпели поражение. Продолжайте рейд.',
            'text_if_success_pupil' => 'Ученик Властера повержен. Продолжайте рейд.',
            'user' => [
                'complete' => 0,
                'enemies_left' => [1,2,3,4,5,6,7],
                'raid' => 1,
                'enemy' => 1,
                'battle' => 0
            ],
            'next' => '1.3'
        ],
        'stage3' => [
            'type' => 'text-with-reward',
            'text' => 'Вы облегчили жизнь многим людям разобравшись с разбойниками, награда 35 вальмер 10000 монет.',
            'button' => 'Завершить главу',
            'cooldown' => 0,
            'reward' => [
                'valmers' => 35,
                'exp' => 10000
            ],
            'user' => [
                'complete' => 1
            ],
            'next' => '2.0'
        ]
    ],
    'chapter2' => [
        'stage1' => [
            'type' => 'text',
            'text' => 'Совсем недавно на деревню Брегги были обрушены силы карт злых учеников Властера, теперь этой деревне нужна помощь в сооружении магической защитной стены. Для этого тебе необходимо отправиться в лес и раздобыть там как можно больше необходимых для сооружения стены ресурсов. Больше всего ресурсов можно раздобыть в заброшенных районах где раньше стояли деревни, в этих деревнях никто уже не обитает и их защитные свойства стен намного слабже, но все таки необходимо с этой защитой разобраться используя свои карты.',
            'button' => 'Отправиться',
            'cooldown' => 60,
            'user' => [
                'complete' => 1
            ],
            'next' => '2.2'
        ],
        'stage2' => [
            'type' => 'village',
            'cooldown' => 0,
            'villages' => [
                null,
                [
                    'health' => 5000,
                    'armor' => 100,
                    'attack' => 0,
                    'max_attacks' => 10,
                    'reward' => [
                        'count' => 10,
                        'exp' => 500,
                        'campaign_exp' => 500
                    ]
                ],
                [
                    'health' => 6000,
                    'armor' => 200,
                    'attack' => 0,
                    'max_attacks' => 10,
                    'reward' => [
                        'count' => 20,
                        'exp' => 900,
                        'campaign_exp' => 600
                    ]
                ],
                [
                    'health' => 8000,
                    'armor' => 300,
                    'attack' => 0,
                    'max_attacks' => 10,
                    'reward' => [
                        'count' => 30,
                        'exp' => 1500,
                        'campaign_exp' => 700
                    ]
                ],
                [
                    'health' => 10000,
                    'armor' => 400,
                    'attack' => 0,
                    'max_attacks' => 10,
                    'reward' => [
                        'count' => 40,
                        'exp' => 2500,
                        'campaign_exp' => 800
                    ]
                ],
                [
                    'health' => 15000,
                    'armor' => 500,
                    'attack' => 0,
                    'max_attacks' => 15,
                    'reward' => [
                        'count' => 50,
                        'exp' => 3500,
                        'campaign_exp' => 900
                    ]
                ],
                [
                    'health' => 20000,
                    'armor' => 600,
                    'attack' => 0,
                    'max_attacks' => 15,
                    'reward' => [
                        'count' => 60,
                        'exp' => 4500,
                        'campaign_exp' => 1000
                    ]
                ],
                [
                    'health' => 40000,
                    'armor' => 600,
                    'attack' => 1,
                    'max_attacks' => 15,
                    'reward' => [
                        'count' => 100,
                        'exp' => 6500,
                        'campaign_exp' => 2500
                    ]
                ],
                [
                    'health' => 60000,
                    'armor' => 600,
                    'attack' => 1,
                    'max_attacks' => 15,
                    'reward' => [
                        'count' => 200,
                        'exp' => 8500,
                        'campaign_exp' => 3500
                    ]
                ],
                [
                    'health' => 80000,
                    'armor' => 600,
                    'attack' => 1,
                    'max_attacks' => 15,
                    'reward' => [
                        'count' => 300,
                        'exp' => 10500,
                        'campaign_exp' => 4500
                    ]
                ],

            ],
            'user' => [
                'complete' => 0,
                'count' => 0,
                'village' => 1,
                'next_village' => 2,
                'cooldown' => 0,
                'attacks' => 10,
                'find' => 1,
                'battle' => 0
            ],
            'next' => '2.3'
        ],
        'stage3' => [
            'type' => 'text-with-reward',
            'text' => 'Жители деревни благодарят вас за оказаную им помощь и не упускают возможности наградить вас! Получено: 50 вальмер, 10000 монет, 2000 рангового опыта!',
            'button' => 'Завершить главу',
            'cooldown' => 0,
            'reward' => [
                'valmers' => 50,
                'campaign_exp' => 2000,
                'exp' => 10000
            ],
            'user' => [
                'complete' => 1,
            ],
            'next' => '3.0'
        ]
    ],
    'chapter3' => [
        'stage1' => [
            'type' => 'text',
            'text' => 'Никто и никогда не скрывал, что на каждую мирную деревню нападают практически всегда злые ученики Властера, дабы нажиться богатствами этих деревень. Твоя задача найти такую деревню, дождаться нападения и отразить его.',
            'button' => 'Отправиться (30 минут)',
            'cooldown' => 30,
            'user' => [
                'complete' => 1
            ],
            'next' => '3.2'
        ],
        'stage2' => [
            'type' => 'opposition',
            'cooldown' => 0,
            'enemies' => [
                null,
                [
                    'health' => 3200,
                    'attack' => 250,
                    'armor' => 200,
                    'reward' => [
                        'valmers' => 1,
                        'campaign_exp' => 50
                    ]
                ],
                [
                    'health' => 4900,
                    'attack' => 300,
                    'armor' => 250,
                    'reward' => [
                        'valmers' => 2,
                        'campaign_exp' => 150
                    ]
                ],
                [
                    'health' => 7500,
                    'attack' => 450,
                    'armor' => 300,
                    'reward' => [
                        'valmers' => 3,
                        'campaign_exp' => 250
                    ]
                ],
                [
                    'health' => 11500,
                    'attack' => 600,
                    'armor' => 450,
                    'reward' => [
                        'valmers' => 4,
                        'campaign_exp' => 350
                    ]
                ],
            ],
            'user' => [
                'complete' => 0,
                'cooldown' => 0,
                'enemy' => 1,
                'find' => 0,
                'battle' => 0,
                'destruct_cooldown' => 0,
                'myHealth' => 0,
                'enemyHealth' => 0,
                'attackCooldown' => 0
            ],
            'next' => '3.3'
        ],
        'stage3' => [
            'type' => 'text-with-reward',
            'text' => 'Вся деревня благодарит вас и не в праве не поделиться с вами своими богатствами! Получено: 40 вальмер, 10000 монет, 5 алмазов!',
            'button' => 'Завершить главу',
            'cooldown' => 0,
            'reward' => [
                'valmers' => 40,
                'diamonds' => 5,
                'exp' => 10000,
                'campaign_exp' => 2000
            ],
            'user' => [
                'complete' => 1,
            ],
            'next' => '4.0'
        ],
    ],
];
