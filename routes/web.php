<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/ads', 'IndexController@ads');
Route::group(['middleware' => 'guest'], function(){
    Route::get('/', 'IndexController@index');
    Route::get('/login', [
        'as' => 'login',
        'uses' => 'IndexController@login'
    ]);
    Route::post('/login/yes', [
        'uses' => 'IndexController@loginConfirm',
        'as' => 'login.confirm'
    ]);
    Route::group(['middleware' => 'register-lock'], function() {
        Route::get('/start', 'IndexController@start');
    });
});
Route::group(['middleware' => ['auth', 'online']], function(){
    Route::group(['middleware' => 'tutorial'], function(){
        Route::get('/game', 'MainController@index');
        Route::get('/postTournament', 'MainController@postTournament');
        Route::get('/faq', 'MainController@faq');
        Route::get('/collectGift', 'MainController@collectGift');
        Route::get('/block', 'MainController@block');
        Route::get('/online', 'MainController@online');
        Route::get('/hideNews', 'MainController@hideNews');
        Route::get('/pay', 'PaymentController@index');
        Route::get('/rating', 'MainController@rating');
        Route::get('/notifications', 'NotificationController@index');
        Route::get('/profile/{id}', 'MainController@profile');
        Route::get('/profile/equipment/{id}', 'ItemsUserController@equipment');
        Route::get('/view_item/{id}', 'ItemsUserController@view_item');
        Route::get('/bag/', 'ItemsUserController@bag');
        Route::get('/bag/put_on/{id}/', 'ItemsUserController@putOnItem');
        Route::get('/action', 'MainController@action');
        Route::get('/quit', 'MainController@quit');
        Route::group(['middleware' => 'register-lock'], function() {
            Route::get('/tutorial/1', 'TutorialController@tutorial1');
            Route::get('/tutorial/2', 'TutorialController@tutorial2');
            Route::get('/tutorial/3', 'TutorialController@tutorial3');
            Route::get('/tutorial/4', 'TutorialController@tutorial4');

        });
        Route::group(['middleware' => 'chat-lock'], function() {
            Route::get('/chat', 'ChatController@index');
            Route::post('/chat/send', [
                'uses' => 'ChatController@sendConfirm',
                'as' => 'chat.send'
            ]);
            Route::get('/chat/reply/{id}', 'ChatController@sendWithReply');
            Route::post('/chat/sendWithReply', [
                'uses' => 'ChatController@sendWithReplyConfirm',
                'as' => 'chat.send.reply'
            ]);
        });
        Route::group(['middleware' => 'mail-lock'], function() {
            Route::get('/mail', 'MailController@index');
            Route::get('/mail/dialog/{id}', 'MailController@dialog');
            Route::post('/mail/send', [
                'uses' => 'MailController@send',
                'as' => 'mail.send'
            ]);
            Route::get('/mail/edit/{id}', 'MailController@edit');
            Route::post('/mail/editConfirm', [
                'uses' => 'MailController@editConfirm',
                'as' => 'mail.edit'
            ]);
            Route::get('/mail/new/{id}', 'MailController@newMessage');
            Route::post('/mail/newSend', [
                'uses' => 'MailController@newMessageConfirm',
                'as' => 'mail.new'
            ]);
            Route::get('/mail/friendlist', 'MailController@friendList');
            Route::get('/mail/blacklist', 'MailController@blackList');
            Route::get('/mail/friendlist/add{id}', 'MailController@addFriendList');
            Route::get('/mail/blacklist/add{id}', 'MailController@addBlackList');
            Route::get('/mail/friendlist/del{id}', 'MailController@delFriendList');
            Route::get('/mail/blacklist/del{id}', 'MailController@delBlackList');
        });

        Route::get('/chests', 'ChestController@index');
        Route::get('/chests/{chest}/open', 'ChestController@open');
        Route::get('/support', 'SupportController@index');
        Route::get('/support/new', 'SupportController@newTicket');
        Route::post('/support/new/yes', [
            'uses' => 'SupportController@newTicketConfirm',
            'as' => 'support.create'
        ]);
        Route::get('/support/ticket/{id}', 'SupportController@ticket');
        Route::post('/support/ticket/{id}/send', 'SupportController@send');
        Route::get('/support/ticket/{id}/close', 'SupportController@close');
        Route::get('/forum', 'ForumController@index');
        Route::get('/forum/cat{id}', 'ForumController@category');
        Route::get('/forum/create', 'ForumController@categoryCreate');
        Route::post('/forum/create/yes', [
            'uses' => 'ForumController@categoryCreateConfirm',
            'as' => 'forum.category.create'
        ]);
        Route::get('/forum/top{topicId}', 'ForumController@topic');
        Route::get('/forum/top{topicId}/changeStatus', 'ForumController@changeStatus');
        Route::get('/forum/cat{categoryId}/create', 'ForumController@topicCreate');
        Route::post('/forum/cat{categoryId}/create/yes', [
            'uses' => 'ForumController@topicCreateConfirm',
            'as' => 'forum.topic.create'
        ]);
        Route::get('/forum/edit{postId}', 'ForumController@editPost');
        Route::post('/forum/edit{postId}/yes', [
            'uses' => 'ForumController@editPostConfirm',
            'as' => 'forum.post.edit'
        ]);
        Route::post('/forum/top{topicId}/post', [
            'uses' => 'ForumController@postCreate',
            'as' => 'forum.post.create'
        ]);
        Route::get('/settings', 'SettingController@index');
        Route::get('/settings/{type}', 'SettingController@setting');
        Route::post('/settings/login/yes', [
            'uses' => 'SettingController@login',
            'as' => 'settings.login'
        ]);
        Route::post('/settings/password/yes', [
            'uses' => 'SettingController@password',
            'as' => 'settings.password'
        ]);
        Route::get('/settings/sex/yes', 'SettingController@sex');


        Route::get('/referal', 'ReferalController@index');
        Route::get('/referal/my', 'ReferalController@myReferals');
        Route::get('/referal/rating', 'ReferalController@referalsRating');
        Route::get('/return', 'AdminController@returnToAccount');
        Route::group(['middleware' => 'role:moders'], function(){
            Route::get('/access', 'AdminController@access');
            Route::post('/access/yes', [
                'uses' => 'AdminController@accessConfirm',
                'as' => 'admin.access'
            ]);
            Route::get('/chat/del/{id}', 'ChatController@del');
            Route::get('/forum/top{topicId}/delete', 'ForumController@topicDelete');
            Route::get('/forum/post{postId}/delete', 'ForumController@postDelete');
            Route::get('/admin', 'AdminController@index');
            Route::get('/admin/support', 'AdminController@supportTickets');
            Route::get('/admin/support/{type}', 'AdminController@supportTicketsType');
            Route::get('/admin/blockUser/{id?}', 'AdminController@blockUser');
            Route::post('/admin/blockUserYes', [
                'uses' => 'AdminController@blockUserConfirm',
                'as' => 'admin.blockUser'
            ]);
            Route::get('/admin/blockList', 'AdminController@blockList');
            Route::get('/admin/blockList/{id}/edit', 'AdminController@editBlock');
            Route::post('/admin/blockList/{id}/edit/yes', [
                'uses' => 'AdminController@editBlockConfirm',
                'as' => 'admin.editBlock'
            ]);
            Route::get('/admin/blockList/{id}/del', 'AdminController@delBlock');
            Route::get('/admin/blockIP', 'AdminController@blockIP');
            Route::post('/admin/blockIP/yes', [
                'uses' => 'AdminController@blockIPConfirm',
                'as' => 'admin.blockIP'
            ]);
        });
        Route::group(['middleware' => 'role:only-admins'], function(){
            Route::get('/admin/addNews', 'AdminController@addNews');
            Route::post('/admin/addNews/yes', [
                'uses' => 'AdminController@addNewsConfirm',
                'as' => 'admin.addNews'
            ]);
            Route::get('/admin/authorize', 'AdminController@authorizeOtherAccount');
            Route::post('/admin/authorize/yes', [
                'uses' => 'AdminController@authorizeOtherAccountConfirm',
                'as' => 'admin.authorize'
            ]);
            Route::get('/admin/gameSetting/change/{type}', 'AdminController@changeSettingStatus');
            Route::get('/admin/gift', 'AdminController@gift');
            Route::post('/admin/gift/yes', [
                'uses' => 'AdminController@giftConfirm',
                'as' => 'admin.gift'
            ]);
            Route::get('/admin/referalTournament', 'AdminController@referalTournament');
            Route::post('/admin/referalTournament/yes', [
                'uses' => 'AdminController@referalTournamentConfirm',
                'as' => 'admin.referalTournament'
            ]);
            Route::get('/admin/deleteUser', 'AdminController@deleteUser');
            Route::post('/admin/deleteUser/yes', [
                'uses' => 'AdminController@deleteUserConfirm',
                'as' => 'admin.deleteUser'
            ]);

            Route::get('/admin/action', 'AdminController@action');
            Route::post('/admin/action/{type}/yes', [
                'uses' => 'AdminController@actionConfirm',
                'as' => 'admin.action'
            ]);
        });
    });
    Route::group(['middleware' => 'register-lock'], function() {
        Route::get('/tutorial/2/{action}', 'TutorialController@tutorial2Confirm');
        Route::get('/tutorial/1/{sex}', 'TutorialController@tutorial1Confirm');
        Route::post('/tutorial/3/yes', [
            'as' => 'tutorial3.confirm',
            'uses' => 'TutorialController@tutorial3Confirm'
        ]);
        Route::get('/tutorial/4/{card}', 'TutorialController@tutorial4Confirm');
        Route::get('/tutorial/5/attack', 'TutorialController@tutorial5Attack');
        Route::get('/tutorial/5/up', 'TutorialController@tutorial5Up');
        Route::get('/tutorial/6/{card}', 'TutorialController@tutorial6Confirm');
        Route::post('/tutorial/7/yes', [
            'as' => 'tutorial7.confirm',
            'uses' => 'TutorialController@tutorial7Confirm'
        ]);


    });
});

Route::get('/referal/{referal}', 'ReferalController@setReferal');
Route::any('/payment/result', 'PaymentController@result');